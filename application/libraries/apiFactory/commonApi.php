<?php

class commonApi
{
    public $CI;

    public function __construct()
    {

        $this->CI = &get_instance();
        $this->CI->load->model('comModel');
        $this->CI->load->model('procedure');

    }

    /**
     * Curl Get 형태로 으로 호출
     * @param $sendUrl
     * @param array $postParams
     * @return bool|string
     */
    public function setCurl($sendUrl, $postParams = array()){

        $curl = curl_init();
        //HTTTPS 인 경우 . SSL 통신
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

        curl_setopt($curl, CURLOPT_URL, $sendUrl);

        curl_setopt($curl, CURLOPT_HEADER, 0);
        if(isset($postParams['coin_type']) && $postParams['coin_type'] == 'ZIL')
        {
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('X-APIKEY:'.$postParams['api_key'].''));
        }
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 600);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT , 600);

        $res = curl_exec($curl);

        curl_close($curl);

        return $res;
    }

    /**
     * Curl Post 형태로 으로 호출
     * @param $params
     * @return bool|string
     */
    public function setPostCUrl($params)
    {
        if(!is_array($params) && !isset($params['postData']))
        {
            return "Error";
        }

        $post_field_string = json_encode($params['postData']);


        $curl = curl_init();
        //HTTTPS 인 경우 . SSL 통신
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

        curl_setopt($curl, CURLOPT_URL, $params['sendUrl']);

        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_field_string);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 600);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT , 600);

        $res = curl_exec($curl);

        curl_close($curl);

        return $res;
    }

    public function setJsonRpc($params)
    {
        if(!is_array($params) && !isset($params['postData']))
        {
            return "Error";
        }

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $params['sendUrl']);

        curl_setopt($curl, CURLOPT_PORT, $params['port']);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        //curl_setopt($curl, CURLOPT_POSTFIELDS, '{"jsonrpc":"2.0","id":1,"method":"getbalance","params":["PIVX_DT",288,true]}');//DL2Qsffu2VkmHyZFtNRyyR49Hx45e1HwjZ
        curl_setopt($curl, CURLOPT_POSTFIELDS, $params['jsonData']);
        curl_setopt($curl, CURLOPT_POST, 1);

        $res = curl_exec($curl);

        curl_close($curl);

        return $res;
    }


    /**
     * 로그 세팅
     * @param $msg
     * @param null $randKey
     */
    public function setLog( $msg , $randKey = null ){
        $msg = '['.date('Y-m-d H:i:s').']['.$randKey.']'.$msg;
        $handle = fopen('application/logs/log-'.date('Y-m-d').'.log', "a+");
        fwrite($handle, $msg.PHP_EOL);
        fclose($handle);
    }

    /**
     * 밸런스 API 호출
     * @param $walletInfo
     */
    public function getBalance($walletInfo ){
        $this->CI->timeKey = md5(strtotime('now').$walletInfo['coin_type']);
        $this->setLog(__METHOD__.'==START' , $this->CI->timeKey);
        ini_set('max_execution_time', 300);
        ini_set('memory_limit','-1');

        $this->CI->divisor = bcpow(10, $walletInfo['coin_decimal']);
    }


    /**
     * API 로 호출한 밸런스를 DB에 저장
     * @param $walletInfo
     * @param $balance
     * @param $timeKey
     * @return mixed
     *
     */
    public function setBalance($balSetParams ,  $timeKey){

        $this->setLog('Params = '.json_encode($balSetParams), $timeKey);

        $this->CI->load->model('procedure');

        $ret = $this->CI->procedure->fnCoinBalanceSave($balSetParams);
        $this->setLog(__METHOD__.'==END', $timeKey);

        return $ret;
    }

    /**
     * Curl 로 요청한 데이터를 JSON으로 받아서 , Array 로 리턴시키는 로직
     * @param $sendUrl
     * @param null $timeKey
     * @return mixed
     */
    public function sendCurl($sendUrl , $timeKey = null , $walletInfo = array()){

        $this->setLog($sendUrl, $timeKey);
        $res = $this->setCurl($sendUrl, $walletInfo);
        $this->setLog('curl result = '.json_encode($res), $timeKey);

        $result = json_decode($res , true);

        return $result;
    }

    /**
     * txid리스트 API 호출
     * @param int $param
     * @param string $coinType
     * @param string $tag_nm
     * @return bool
     */
    public function getTxList($coinInfo){

        if( empty($coinType) || empty($tag_nm) ){
            return false;
        }

        ini_set('max_execution_time', 300);
        ini_set('memory_limit','-1');

        $this->CI->coinType = strtoupper($coinInfo['coin_type']);

        $this->CI->divisor = bcpow(10, $coinInfo['coin_decimal']);

        if(!isset($coinInfo['addr']) || empty($coinInfo['addr']))
        {
            echo "동기화 대상이 없습니다.";
            return false;
        }

        $this->CI->syncParam = array(
            ":coin_type" => $coinInfo['coin_type'],
            ":tag_nm" =>$coinInfo['tag_nm'],
            //":last_block" => $param,
            ":sync_state" => "S",
        );
    }

    /**
     * API로 호출한 txid 리스트를 저장
     * @param $rgParams
     * @return mixed
     **/
    public function setTxList( $rgParams ){

        $procResult = $this->CI->procedure->fnDaemonLogProc($rgParams);

        $errMsg = array(
            0 => "카운트는 성공 insert error",
            1 => "성공",
            -1 => "쿼리 오류",
            -2 => "중복 Txid",
        );

        if($procResult == 1) {
            return true;
        }else{

            $result['result'] = array(
                "success" => FALSE,
                "message" => $errMsg[$procResult]
            );
        }

        return $result;
    }

    public function getLastSyncBlock( $syncParam ){

        $lastInfo = $this->CI->comModel->lastBlock($syncParam);
        return $lastInfo[0]["last_block"];
    }

    public function changeSync( $syncParam){

        return $this->CI->comModel->changeSync($syncParam);

    }

    public function totDaemonCnt($coinType , $tagNm){
        $params = array(
            "coinType" => $coinType,
            "tag_nm" => $tagNm
        );

        $dbCNt = $this->CI->comModel->totDaemonCnt($params);
        $mxDate = strtotime($dbCNt[0]['reg_dt']);
        $cnt = $dbCNt[0]['CNT'];

        return array(
            'mxDate' => $mxDate,
            'cnt' => $cnt
        );
    }

    public function checkCategory($val){

        if( $val['parent'] == 'EOS' ){

        }
    }

}