<?php


class trueApi extends commonApi
{

    public function getBalance($walletInfo)
    {
        parent::getBalance($walletInfo);

        $sendUrl = "https://api.truescan.net/main/account?address=".$walletInfo['addr'];
        $res = $this->sendCurl($sendUrl , $this->CI->timeKey);

        $balance =  bcdiv($res['data']['balance'], $this->CI->divisor, $walletInfo['coin_decimal']) ;

        return $this->setBalance($walletInfo , $balance , $this->CI->timeKey);
    }

    public function getTxList($walletInfo)
    {
        parent::getTxList($walletInfo);
        $param = $walletInfo['startBlock'];
        $coinType = $walletInfo['coin_type'];
        $tag_nm = $walletInfo['tag_nm'];

        $sendUrl = "https://api.truescan.net/main/";
        $subUrl = "txs?page=0&size=10&address=".$walletInfo['addr'];

        $res = $this->sendCurl($sendUrl.$subUrl);

        $totalCnt = $res['data'][1];
        $totPageSize = floor($totalCnt/100);

        $lastBlock = $param;

        if($param = 0 ) {
            $lastBlock = $this->getLastSyncBlock($this->CI->syncParam);
        }

        $page = $totPageSize-$lastBlock;
        $subUrl = "txs?page={$page}&size=10&address=".$walletInfo['addr'];

        $res = $this->sendCurl($sendUrl.$subUrl);

        $result = $res['data'][0];

        if(!empty($result)){

            krsort($result);

            foreach ($result as $key => $val){

                $type = ($val['from'] == $walletInfo['addr']) ? "send" : "receive";

                $value = hexdec($val['value']);
                $value = sprintf('%f', $value);
                $balance = bcdiv($value, $this->CI->divisor, $walletInfo['coin_decimal']);

                $fee = $val['gasUsed'];

                $rgParams = array(
                    "coinType" => $coinType ,
                    "category" => $type,
                    "parent" => $this->CI->coinInfo[0]['parent'],
                    "mainType" => $this->CI->coinInfo[0]['main_type'],
                    "subType" => $this->CI->coinInfo[0]['sub_type'],
                    "tagNm"   => $walletInfo['tag_nm'],
                    "blockNumber" => $val['number'],
                    "from" => $val['from'],
                    "to" => $val['to'],
                    "txid" => $val['txHash'],
                    "amount" => $balance,
                    "fee" => bcdiv($fee, $this->CI->divisor, $walletInfo['coin_decimal']),
                    "date" => date('Y-m-d H:i:s.u' , strtotime($val['timestamp'])),
                );

                $this->setTxList($rgParams);
            }

        }

        if($totPageSize >= $lastBlock+1) {

            $this->CI->syncParam[':last_block'] = $lastBlock + 1;
            $this->CI->syncParam[':sync_state'] = 'S';

            $this->changeSync($this->CI->syncParam);

            return $this->getTxList($lastBlock + 1, $coinType, $tag_nm);

        }else{

            $this->CI->syncParam[':last_block'] = $lastBlock;
            $this->CI->syncParam[':sync_state'] = 'Y';

            $this->changeSync($this->CI->syncParam);
            exit;
        }

    }
}