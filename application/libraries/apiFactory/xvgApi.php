<?php


class xvgApi extends commonApi
{
    public function getBalance($walletInfo)
    {
        parent::getBalance($walletInfo);

        $sendUrl ='https://verge-blockchain.info/ext/getbalance/'.$walletInfo['addr'];
        $balance = $this->sendCurl($sendUrl , $this->CI->timeKey);

        return $this->setBalance($walletInfo , $balance , $this->CI->timeKey);

    }

    public function getTxList($walletInfo)
    {
        parent::getTxList($walletInfo);
        $param = $walletInfo['startBlock'];
        $coinType = $walletInfo['coin_type'];
        $tag_nm = $walletInfo['tag_nm'];

    }
}