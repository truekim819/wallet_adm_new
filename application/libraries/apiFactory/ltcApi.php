<?php


class ltcApi extends commonApi
{

    function getBalance($walletInfo)
    {
        parent::getBalance($walletInfo);
        ini_set('max_execution_time', 300);

        $sendUrl = "http://explorer.litecoin.net/chain/Litecoin/q/addressbalance/".$walletInfo['addr'];
        $res = $this->sendCurl($sendUrl , $this->CI->timeKey);

        $balance = isset($res) ? $res : '0';

        return $this->setBalance($walletInfo , $balance , $this->CI->timeKey);
    }

    public function getTxList($walletInfo)
    {
        parent::getTxList($walletInfo);
        $param = $walletInfo['startBlock'];
        $coinType = $walletInfo['coin_type'];
        $tag_nm = $walletInfo['tag_nm'];
    }
}