<?php


class steemApi extends commonApi
{
    public function getBalance($walletInfo)
    {
        parent::getBalance($walletInfo);
        ini_set('max_execution_time', 300);

        $postData = array(
            "sendUrl" => "https://api.steemit.com",
            "postData" => array(
                "jsonrpc"=>"2.0",
                "method"=>"database_api.find_accounts",
                "params"=>array
                (
                    "accounts" => array($walletInfo['addr']),
                ),
                "id" => 1,
            ),
        );

        $temp = $this->setPostCUrl( $postData );
        $res  = json_decode($temp  , 256);

        $balance = bcdiv($res['result']['accounts'][0]['balance']['amount'], $this->CI->divisor, $walletInfo['coin_decimal']);
        return $this->setBalance($walletInfo , $balance , $this->CI->timeKey);

    }

    public function getTxList($walletInfo)
    {
        parent::getTxList($walletInfo);
        $param = $walletInfo['startBlock'];
        $coinType = $walletInfo['coin_type'];
        $tag_nm = $walletInfo['tag_nm'];
    }


}