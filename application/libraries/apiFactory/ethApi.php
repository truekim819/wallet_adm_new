<?php


class ethApi extends commonApi
{

    public function getBalance($walletInfo){
        parent::getBalance($walletInfo);

        $coinAddr = $walletInfo['addr'];

        $balArr = array(
            "module" => "account",
            "action" => "balance",
            "address" => $coinAddr,
            "tag"=>"latest",
            "apikey" => ETHER_API_KEY
        );

        if($walletInfo['parent'] != $walletInfo['coin_type'] ) {
            $balArr["action"] = "tokenbalance";
            $balArr["contractaddress"] = $walletInfo['extra_addr'];
         }

        unset($res);
        $balanceUrl = ETHER_URL.http_build_query($balArr);

        $res = $this->sendCurl($balanceUrl , $this->CI->timeKey);
        $balance = bcdiv($res['result'] ,  $this->CI->divisor , $walletInfo['coin_decimal']);

        return $this->setBalance($walletInfo , $balance , $this->CI->timeKey);
    }

    public function getTxList($walletInfo)
    {
        parent::getTxList($walletInfo);
        $param = $walletInfo['startBlock'];
        $coinType = $walletInfo['coin_type'];
        $tag_nm = $walletInfo['tag_nm'];

        $this->setLog("/////////////////////////////////////////////////////////////////////////////////" , $tag_nm.'_log');
        $this->setLog($param.'=>'.__LINE__ , $tag_nm.'_log');
        $tokenStatus = false;

        if($this->CI->coinInfo[0]['extra_addr'] != '')
        {
            $tokenStatus = true;
        }

        $apiList = ($coinType == 'ETH')?(strpos($tag_nm,"_int_")?"txlistinternal":"txlist"):"tokentx";
        $lastBlock = $this->getBlock();

        $startblock = ($param == 0)? 0 : $param;
        $endblock = 99999999;//$lastBlock;//($param == 0) ? 99999999: ($param+100);
        $offset = 500;//($endblock == 99999999) ? 1 : 500;

        $postParam = array(
            "module" => "account",
            "action" => $apiList, //txlist
            "address" => $this->CI->coinAddr,
            "startblock" => $startblock,
            "endblock" => $endblock, //99999999
            "page" => 1,
            "offset" => $offset,
            "sort" => "asc", //desc
            "apikey" => ETHER_API_KEY
        );

        $bindQuery = http_build_query($postParam);

        $sendUrl = ETHER_URL.$bindQuery;

        $res = $this->sendCurl($sendUrl);
        $result = $res['result'];
        $message = $res['message'];

        //if($lastBlock < (int)$startblock && count($result)==0)
        if($lastBlock < (int)$startblock)
        {
            $this->setLog($param.'=>'.__LINE__ , $tag_nm.'_log');
            $this->CI->syncParam[":sync_state"] = "Y";
            $this->CI->syncParam[":last_block"] = $lastBlock;
            $this->CI->comModel->changeSync($this->CI->syncParam);
            $retData['result'] = array(
                "success" => TRUE,
                "message" => "동기화 완료 되었습니다"
            );

            unset($this->CI->coinInfo);

            return $retData;
        }

        if($message == "No transactions found")
        {
            $this->setLog($param.'=>'.__LINE__ , $tag_nm.'_log');
            $nextBlock = $lastBlock+1;
            $this->CI->syncParam[":last_block"] = $nextBlock;
            $this->CI->comModel->changeSync($this->CI->syncParam);
            return $this->getTxList((int)$nextBlock, $coinType , $tag_nm);
        }

        if($startblock == 0){
            $this->CI->syncParam[":last_block"] = $result[0]['blockNumber'];
            $this->CI->comModel->changeSync($this->CI->syncParam);
            return $this->getTxList((int)($result[0]['blockNumber']),$coinType, $tag_nm);
        }

        $i = 0;
        foreach ($result as $key => $val )
        {
            $startblock = $val['blockNumber'];
            if($tokenStatus == true)
            {
                if(strlen($val['tokenSymbol'])>10)
                {
                    continue;
                }

                if($val['tokenSymbol'] != $this->CI->coinInfo[0]['coin_type']){
                    continue;
                }
            }

            $val['coinType'] = (isset($val['tokenSymbol']) && !empty($val['tokenSymbol']))? $val['tokenSymbol']: $coinType;
            $val['date'] = date('Y-m-d H:i:s.u' , $val['timeStamp']);
            $val['category'] = (strtolower($this->CI->coinAddr) == $val['to'])? "receive" : "send";
            $val['parent'] = $this->CI->coinInfo[0]['parent'];
            $val['mainType'] = $this->CI->coinInfo[0]['main_type'];
            $val['subType'] = $this->CI->coinInfo[0]['sub_type'];
            $val['tagNm'] = $tag_nm;
            $val['txid'] =  $val['hash'];
            $decimal = 18;
            if(isset($val['tokenDecimal'])){
                $decimal = $val['tokenDecimal'];
            }
            $val['amount'] =  bcdiv($val['value'], bcpow(10,$decimal) , (int)$decimal);
            $val['fee'] =  bcdiv($val['gas'], ETHER , 18);
            $val['from_addr'] = $val['from'];
            $tokenAddr = $val['contractAddress'];

            $this->setTxList($val);
        }

        $tblName = strtolower($val['coinType']);
        $tblName = preg_replace("/[^a-zA-Z]/s", '', $tblName);

        if($tokenStatus === false && $coinType == $tblName)
        {
            $rgParams = array(
                ":coinType" => $tblName,
                ":contAddr" => $tokenAddr,
                ":walletType" => $this->CI->coinInfo[0]['wallet_type'],
            );
            $this->CI->comModel->setContAddress($rgParams);
        }

        $this->CI->syncParam[':last_block'] = $startblock+1;
        $this->CI->comModel->changeSync($this->CI->syncParam);

        $this->setLog($param.'=>'.__LINE__ , $tag_nm.'_log');
        return $this->getTxList((int)($startblock+1),$coinType , $tag_nm);
    }

    public function getBlock()
    {
        $postParam = array(
            "module" => "proxy",
            "action" => "eth_blockNumber",
            "apikey" => API_KEY
        );

        $bindQuery = http_build_query($postParam);

        $sendUrl = ETHER_URL.$bindQuery;
        $res = $this->setCurl($sendUrl);
        $res = json_decode($res);
        $blockNumber = hexdec($res->result);
        return $blockNumber;
    }
}