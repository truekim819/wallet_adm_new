<?php


class pivxApi extends commonApi
{

    var $url= "http://test1:test12345@192.168.1.50";
    public function getBalance($walletInfo=array())
    {
        $jsonParams = array(
            "sendUrl" => $this->url,
            "port" => 51473,
            "jsonData" => '{"jsonrpc":"2.0","id":1,"method":"listaddressgroupings","params":[]}'
        );

        $res = $this->setJsonRpc($jsonParams);
        $res  = json_decode($res  , true);

        $balanceList = array();
        foreach($res['result'] as $group){
            $balanceAmt = 0;
            foreach($group as $val){
                $balanceList[$val[2]] = isset($balanceList[$val[2]]) ? bcadd($balanceList[$val[2]]  , $val[1] , 8) : 0;
            }
        }

        foreach ($balanceList as $tagNm => $bal){

            $balanceInfo = array(
                "balanceAmt" => $bal,
                "stakingAmt" => 0,
                "confirmAmt" => 0,
                "unconfirmAmt" => 0,
                "tagNm" => $tagNm,
                "timeKey" => $walletInfo["groupkey"],
            );

            $ret = $this->setBalance( $balanceInfo , date('Y-m-d H:i:s'));



        }
        //return
    }

    public function getTxList($walletInfo)
    {
        parent::getTxList($walletInfo);
        $jsonParams = array(
            "sendUrl" => $this->url,
            "port" => 51473,
            "jsonData" => '{"jsonrpc":"2.0","id":1,"method":"listtransactions","params":["*", 100 , '.$walletInfo['startBlock'].' , true]}'
        );

        $res = $this->setJsonRpc($jsonParams);
        $res  = json_decode($res  , true);

        $result = $res['result'];

        if(count($result)>0) {
            rsort($res['result']);
        }else{
            //더이상 쌓을 트랜잭션 없음
            return array(

            );
        }

        foreach($result as $val ){

            $val['coinType']    = $walletInfo['coin_type'];
            $val['parent']      = $walletInfo['parent'];
            $val['mainType']    = $walletInfo['main_type'];
            $val['subType']     = $walletInfo['sub_type'];
            $val['tagNm']       = !empty($val['account']) ? $val['account'] : "PIVX_CHANGE";
            $val['from']        = $val['category'] == 'send' ? $val['address'] : null;
            $val['to']          = $val['category'] == 'receive' ? $val['address'] : null;
            $val['date']        = date('Y-m-d H:i:s' , $val['blocktime']);
            $val['blockNumber'] = 0;
            $val['trade_amt']   = $val['amount'];

            $val['receive_amt']   = 0;
            $val['sent_amt']   = 0;

            $this->setTxList($val);

            $jsonParams = array(
                "sendUrl" => $this->url,
                "port" => 51473,
                "jsonData" => '{"jsonrpc":"2.0","id":1,"method":"gettransaction","params":["'.$val['txid'].'"]}'
            );

            $tx = $this->setJsonRpc($jsonParams);
            $tx  = json_decode($tx  , true);

            $hex = $tx['result']['hex'];

            $jsonParams = array(
                "sendUrl" => $this->url,
                "port" => 51473,
                "jsonData" => '{"jsonrpc":"2.0","id":1,"method":"decoderawtransaction","params":["'.$hex.'"]}'
            );

            $hexInfo = $this->setJsonRpc($jsonParams);
            $hexInfo = json_decode($hexInfo  , true);
            if($val['category'] == 'send') {
                $hexlist = $hexInfo['result']['vout'];

                $divValue = pow(10, 8);
                $val['trade_amt'] = 0;
                rsort($hexlist);
                foreach ($hexlist as $info) {
                    $val['fee'] = 0;
                    $val['receive_amt'] = 0;
                    $val['sent_amt']   = bcdiv($info['value'] ,$divValue ,8);
                    $val['vout']    = $info['n'];
                    $val['to']    = $info['scriptPubKey']['addresses'][0];

                    $this->setTxList($val);
                }

            }
        }

        return ;

    }
}