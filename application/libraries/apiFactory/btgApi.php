<?php


class btgApi extends commonApi
{

    public function getBalance($walletInfo)
    {
        parent::getBalance($walletInfo);

        $sendUrl = "https://explorer.bitcoingold.org/insight-api/addr/".$walletInfo['addr']."/balance";
        $res = $this->sendCurl($sendUrl , $this->CI->timeKey);

        $balance = bcdiv($res, $this->CI->divisor, $walletInfo['coin_decimal']);

        return $this->setBalance($walletInfo , $balance , $this->CI->timeKey);

    }

    public function getTxList($walletInfo)
    {
        parent::getTxList($walletInfo);

    }

}