<?php


class adaApi extends commonApi
{
    public function getBalance($walletInfo)
    {
        parent::getBalance($walletInfo);
        $sendUrl = "https://cardanoexplorer.com/api/addresses/summary/".$walletInfo['addr'];
        $res = $this->sendCurl($sendUrl , $this->CI->timeKey);

        $balance = 0;

        if(is_array($res['Right']))
        {
            $balance = $res['Right']['caBalance']['getCoin'];
            $balance = bcdiv($balance , $this->CI->divisor , $walletInfo['coin_decimal']);
        }

        return $this->setBalance($walletInfo , $balance , $this->CI->timeKey);
    }

    public function getTxList($walletInfo)
    {
        parent::getTxList($walletInfo);

    }

}