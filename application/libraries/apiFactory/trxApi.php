<?php


class trxApi extends commonApi
{

    public function getBalance($walletInfo)
    {
        parent::getBalance($walletInfo);
        ini_set('max_execution_time', 300);

        $sendUrl = "https://apilist.tronscan.org/api/account?sort=-balance&address=".$walletInfo['addr'];
        $res = $this->sendCurl($sendUrl , $this->CI->timeKey);

        $intNum = '0';

        if($walletInfo['coin_type'] != $walletInfo['parent']){
            foreach($res['tokenBalances'] as $row)
            {
                if($row['name']==$walletInfo['cont_addr'])
                {
                    $intNum = sprintf('%F', $row['balance']);
                    $intNum = preg_replace('/\\.[0-9]+$/', '', $intNum);
                }
            }
        }else {
            $intNum = sprintf('%F', $res['balance']);
            $intNum = preg_replace('/\\.[0-9]+$/', '', $intNum);
        }

        $balance = bcdiv($intNum, $this->CI->divisor, $walletInfo['coin_decimal']);

        return $this->setBalance($walletInfo , $balance , $this->CI->timeKey);
    }

    public function getTxList($walletInfo)
    {
        parent::getTxList($walletInfo);
        $param = $walletInfo['startBlock'];
        $coinType = $walletInfo['coin_type'];
        $tag_nm = $walletInfo['tag_nm'];
        $tag_nm = $walletInfo['tag_nm'];

    }


}