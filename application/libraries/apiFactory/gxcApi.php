<?php


class gxcApi extends commonApi
{
    public function getBalance($walletInfo)
    {
        parent::getBalance($walletInfo);
        ini_set('max_execution_time', 300);

        $sendUrl = "https://block.gxb.io/api/account/".$walletInfo['addr'];

        $res = $this->sendCurl($sendUrl , $this->CI->timeKey);

        $balance = bcdiv($res['balances'][0]['balance'],$this->CI->divisor, $walletInfo['coin_decimal']);
        return $this->setBalance($walletInfo , $balance , $this->CI->timeKey);
    }

    public function getTxList($walletInfo)
    {
        parent::getTxList($walletInfo);
        $param = $walletInfo['startBlock'];
        $coinType = $walletInfo['coin_type'];
        $tag_nm = $walletInfo['tag_nm'];
    }

}