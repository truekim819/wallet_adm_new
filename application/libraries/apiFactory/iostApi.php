<?php


class iostApi extends commonApi
{
    public function getBalance($walletInfo)
    {
        parent::getBalance($walletInfo);

        $sendUrl = "https://www.iostabc.com/endpoint/getAccount/".$walletInfo['addr']."/0";
        $res = $this->sendCurl($sendUrl , $this->CI->timeKey);

        $balance = $res['balance'];

        return $this->setBalance($walletInfo , $balance , $this->CI->timeKey);
    }

    public function getTxList($walletInfo)
    {
        parent::getTxList($walletInfo);
        $param = $walletInfo['startBlock'];
        $coinType = $walletInfo['coin_type'];
        $tag_nm = $walletInfo['tag_nm'];
    }
}