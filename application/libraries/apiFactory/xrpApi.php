<?php

class xrpApi extends commonApi
{

    public function getBalance($walletInfo ){

        parent::getBalance($walletInfo);

        $sendUrl = "https://data.ripple.com/v2/accounts/".$walletInfo['addr']."/balances";
        $res = $this->sendCurl($sendUrl , $this->CI->timeKey);

        $balance = '0';
        if(!empty($res['balances'][0]['value']) && isset($res['balances'][0]['value']))
        {
            $balance = $res['balances'][0]['value'];
        }

        return $this->setBalance($walletInfo , $balance , $this->CI->timeKey);
    }

    public function getTxList($walletInfo)
    {

        $param = $walletInfo['startBlock'];
        $coinType = $walletInfo['coin_type'];
        $tag_nm = $walletInfo['tag_nm'];

        $paramInfo = explode('|' , $param);
        $isSingle = false;

        if($paramInfo[0] == 'single'){
            $isSingle =true;
            $param = $paramInfo[1];
        }

        parent::getTxList($walletInfo);

        $this->CI->syncParam = array(
            ":coin_type" => $coinType,
            ":tag_nm" => $tag_nm,
            ":last_block" => "",
            ":sync_state" => "S",
        );

        $now = strtotime(date('Y-m-d'));

        if($param != 0 && strtotime($param) >= $now)
        {
            $this->CI->syncParam[':sync_state'] = "Y";
            $this->changeSync($this->CI->syncParam);
            echo "동기화 대상이 없습니다.";
            exit;
        }

        $postParam = array(
            "type" => "Payment",
            "limit" => 1000,
            "result" => "tesSUCCESS",
        );

        $endDay = 0;

        if($param != 0)
        {
            $txParam = array
            (
                'coinType' => $coinType,
                'tagNm' => $tag_nm,
                'strNo' => 0,
                'pageSize' => 1,
            );

            $txInfo = $this->CI->comModel->selectTxList($txParam);

            $reg_dt = (strtotime($txInfo[0]['block_dt']) < strtotime($param.' 00:00:00'))? $param : substr($txInfo[0]['block_dt'] , 0, 10);

/*
            if( $isSingle ){
                $startDay = date("Y-m-d", strtotime($reg_dt.' 00:00:00'));
                $endDay= date("Y-m-d", strtotime($reg_dt.' 23:59:59'));

            }else{
                $startDay = date("Y-m-d", strtotime($reg_dt.' 00:00:00'));
                $endDay= date("Y-m-d", strtotime($reg_dt.' 00:00:00'." +1 day"));

            }
*/
            $startDay = date("Y-m-d", strtotime($reg_dt.' 00:00:00'));
            $endDay= date("Y-m-d", strtotime($reg_dt.' 23:59:59'));

            $postParam["start"] = $startDay;
            $postParam["end"] = $endDay;
        }

        $bindQuery = http_build_query($postParam);

        $coinAddr = isset($this->CI->coinInfo[0]['addr']) ? $this->CI->coinInfo[0]['addr'] : '';

        if(!isset($coinAddr) && empty($coinAddr))
        {
            echo "동기화 대상이 없습니다.";
            exit;
        }

        $sendUrl = "https://data.ripple.com/v2/accounts/".$coinAddr."/transactions/?".$bindQuery;

        $res = $this->sendCurl($sendUrl);

        if($res['result'] != "success")
        {
            echo "동기화  진행 오류 발생 확인해주세요.</br>".$sendUrl."</br>";
            exit;
        }

        $addDay = " +1 day";

        if($res['result'] == "success" && $res['count']==0)
        {
            $this->CI->syncParam[":last_block"] = $endDay;
            $this->changeSync($this->CI->syncParam);
            return $this->getTxList(date("Y-m-d", strtotime($endDay.' 00:00:00'.$addDay)), $coinType, $tag_nm);
        }

        foreach ($res['transactions'] as $row)
        {
            $date = date('Y-m-d H:i:s.u' , strtotime($row['date']));
            if(is_array($row['tx']['Amount']))
            {
                continue;
            }
            $category = ($row['tx']['Destination'] == $coinAddr)? 'receive' : 'send';

            $decimal = $this->CI->coinInfo[0]['coin_decimal'];
            $divisor = bcpow(10, $decimal);

            $rgParams = array(
                "coinType" => $coinType,
                "category" => $category,
                "parent" => $this->CI->coinInfo[0]['parent'],
                "mainType" => $this->CI->coinInfo[0]['main_type'],
                "subType" => $this->CI->coinInfo[0]['sub_type'],
                "tagNm" => $tag_nm,
                "blockNumber" => $row['ledger_index'],
                "from" => $row['tx']['Account'],
                "to" => $row['tx']['Destination'],
                "txid" => $row['hash'],
                "amount" => bcdiv($row['tx']['Amount'], $divisor, $decimal),
                "size" => 0,
                "fee" => bcdiv($row['tx']['Fee'], $divisor, $decimal),
                "date" => $date,
            );

            $this->setTxList($rgParams);
        }

        $nextDay = date("Y-m-d", strtotime($date.$addDay));
        $this->CI->syncParam[":last_block"] = $nextDay;
        $this->changeSync($this->CI->syncParam);

        if($isSingle){
            return array(
                'success' => true,
                'message' => $param."일자 데이터 동기화가 완료됐습니다."
            );

        }else{
            return $this->getTxList($nextDay, $coinType, $tag_nm);
        }
    }

}