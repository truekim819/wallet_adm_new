<?php


class bchApi extends commonApi
{
    public function getBalance($walletInfo)
    {
        parent::getBalance($walletInfo);

        $addr = str_replace("bitcoincash:", "", $walletInfo['addr']);
        $sendUrl = "https://api.blockchair.com/bitcoin-cash/dashboards/address/".$addr;

        $res = $this->sendCurl($sendUrl , $this->CI->timeKey);

        $balance = isset($res['data'][$addr]) ? bcdiv($res['data'][$addr]['address']['balance'], $this->CI->divisor, $walletInfo['coin_decimal']) : '0';

        return $this->setBalance($walletInfo , $balance , $this->CI->timeKey);

    }


    public function getTxList($walletInfo)
    {
        parent::getTxList($walletInfo);

    }

}