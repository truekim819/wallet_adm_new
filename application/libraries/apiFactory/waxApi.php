<?php


class waxApi extends commonApi
{
    public function getBalance($walletInfo)
    {
        parent::getBalance($walletInfo);
        ini_set('max_execution_time', 300);

        $sendUrl = "https://wax.greymass.com/v1/chain/get_currency_balance";

        $postParams = array(
            "sendUrl" => $sendUrl,
            "postData" => array(
                "account" => $walletInfo['addr'],
                "code" => "eosio.token",
                "symbol" => "WAX"
            )
        );
        $res = $this->setPostCUrl($postParams);
        $balance = strtolower(' '.$walletInfo['coin_type'], '' , json_decode($res));


        if(count($balance)>0)
        {
            $this->setBalance($walletInfo , $balance , $this->CI->timeKey);
        }

        return true;
    }
}