<?php


class eosApi extends commonApi
{
    public function getBalance($walletInfo)
    {
        parent::getBalance($walletInfo);
        ini_set('max_execution_time', 300);

        $sendUrl = "https://api.light.xeos.me/api/account/eos/".$walletInfo['addr'];
        $res = $this->sendCurl($sendUrl , $this->CI->timeKey);

        if(count($res['balances'])>0)
        {
            foreach($res['balances'] as $key => $val)
            {
                if( $val['currency'] != $walletInfo['coin_type']){
                    continue;
                }
                $walletType = $walletInfo['wallet_type'];
                $walletInfo['coin_type'] = $val['currency'];
                $walletInfo['wallet_type'] = $walletType;

                $this->setBalance($walletInfo , $val['amount'] , $this->CI->timeKey);

                if( $val['currency'] == $walletInfo['coin_type']){
                    break;
                }
            }
        }

        $staked = bcdiv(bcadd($res['resources']['cpu_weight'], $res['resources']['net_weight']), 10000, 4);

        $walletInfo['tag_nm'] = $walletInfo['tag_nm'].'_staked';
        $walletInfo['coin_type'] = 'EOS';
        $walletInfo['sub_type'] = 'T';
        $this->setBalance($walletInfo, $staked, $this->CI->timeKey);

        return true;
    }


    public function getTxList($walletInfo)
    {
        parent::getTxList($walletInfo);
        $param = $walletInfo['startBlock'];
        $coinType = $walletInfo['coin_type'];
        $tag_nm = $walletInfo['tag_nm'];

        $cnt = 0;
        $mxDate = 0;
        $chkCnt = 0;

        $tokenList = $this->CI->comModel->getEosTokenList();

        $eosToken = "EOS|EOSDAC|";
        foreach($tokenList as $list)
        {
            $eosToken .= $list['coin_type']."|";
        }
        $eosToken = substr($eosToken,0,-1);

        if($param > 0)
        {
            $totDaemonCnt = $this->totDaemonCnt($coinType, $tag_nm);
            $mxDate = $totDaemonCnt['mxDate'];
            $cnt = $totDaemonCnt['cnt'];
        }

        $rgParams = array(
            "sendUrl" => "https://eos.greymass.com/v1/history/get_actions",
            "postData" => array(
                "account_name" => $this->CI->coinAddr,
                "pos" => -1,
                "offset" => -20,
            ),
        );

        $res = $this->setPostCUrl($rgParams);
        $totCnt = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $res));
        $eosLast = $totCnt->actions;
        rsort($eosLast);

        //마지막 eos 받은 날을 찾기 위함.
        foreach($eosLast as $lastDay)
        {
            if($lastDay->action_trace->act->name == "transfer")
            {
                $txTotData = $lastDay->block_time;
                break;
            }
        }

        if(!isset($txTotData))
        {
            $objDateTime = new DateTime('NOW');
            $txTotData = $objDateTime->format('c');
        }

        if(!isset($totCnt->actions[0]->account_action_seq))
        {
            $this->CI->syncParam[":sync_state"] = "N";
            $result = $this->changeSync($this->CI->syncParam);

            return array(
                'result' => array(
                    'success' => false,
                    'message' =>"API Cnt Error"
                )
            );
        }

        $txTotData = strtotime($txTotData);

        $offSet = ($mxDate >= $txTotData)? $param : $param + 100;

        $this->CI->syncParam = array(
            ":coin_type" => $coinType,
            ":tag_nm" => $tag_nm,
            ":last_block" => $cnt,
            ":sync_state" => "S",
        );

        if($param != 0 && $mxDate >= $txTotData)
        {
            $this->CI->syncParam[":sync_state"] = "Y";
            $this->changeSync($this->CI->syncParam);
            return array(
                'result' => array(
                    'success' => true,
                    'message' =>"동기화 완료"
                )
            );
        }

        $rgParams = array(
            "sendUrl" => "https://eos.greymass.com/v1/history/get_actions",
            "postData" => array(
                "account_name" => $this->CI->coinAddr,
                "pos" => $offSet,
                "offset" => -100,
            ),
        );

        $res = $this->setPostCUrl($rgParams);
        $res = json_decode($res);

        $i = 0;
        foreach($res->actions as $row)
        {
            if(!in_array($row->action_trace->act->name, array("transfer","delegatebw","newaccount","buyrambytes","buyram")))
            {
                continue;
            }

            $category = $row->action_trace->act->name;
            $coinCopy = $coinType;

            if($row->action_trace->act->name == "transfer")
            {
                //if (preg_match("/(".$eosToken.")/", $row->action_trace->act->data->quantity, $match))
                $array = explode(" ",$row->action_trace->act->data->quantity);
                $coinCopy = $array[1];

                $from =$row->action_trace->act->data->from;
                $to = $row->action_trace->act->data->to;
                $category = ($from == $this->CI->coinAddr)? "send":"receive";
                $amount = str_replace(" ".$coinCopy,"",$row->action_trace->act->data->quantity);
            }
            else if($row->action_trace->act->name == "delegatebw")
            {
                if(!in_array($row->action_trace->act->account, array("eosio.token","eosio")))
                {
                    continue;
                }

                $from =$row->action_trace->act->data->from;
                $to = $row->action_trace->act->data->receiver;
                $amount = (str_replace(" EOS","",$row->action_trace->act->data->stake_net_quantity)+str_replace(" EOS","",$row->action_trace->act->data->stake_cpu_quantity));
            }
            else if($row->action_trace->act->name == "newaccount")
            {
                $from =$row->action_trace->act->data->creator;
                $to =$row->action_trace->act->data->name;
                $amount = 0;
            }
            else if(in_array($row->action_trace->act->name, array("buyrambytes","buyram")))
            {
                $max = count($row->action_trace->inline_traces);

                $from =$row->action_trace->act->data->payer;
                $to = $row->action_trace->act->data->receiver;
                $amount = str_replace(" EOS","",$row->action_trace->inline_traces[$max-1]->act->data->quantity);
            }

            $rgParams = array(
                "type" => "eos",
                "coinType" => $coinCopy,
                "category" => $category,
                "parent" => $this->CI->coinInfo[0]['parent'],
                "mainType" => $this->CI->coinInfo[0]['main_type'],
                "subType" => $this->CI->coinInfo[0]['sub_type'],
                "tagNm" => $tag_nm,
                "blockNumber" => $row->block_num,
                "from" => $from,
                "to" => $to,
                "txid" => $row->action_trace->trx_id,
                "amount" => $amount,
                "fee" => 0,
                "date" => date('Y-m-d H:i:s.u' , strtotime($row->action_trace->block_time.' + 9 hours')),
            );

            $result = $this->setTxList($rgParams);

            if($result){
                if(preg_match("/(".$eosToken.")/", $coinCopy))
                {
                    $i++;
                }
            }else
            {
                $chkCnt++;
            }
        }

        $this->CI->syncParam[":last_block"] = $cnt + $i;
        $this->changeSync($this->CI->syncParam);
        return $this->getTxList($offSet, $coinType, $tag_nm);
    }

}