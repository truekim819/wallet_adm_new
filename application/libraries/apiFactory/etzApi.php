<?php


class etzApi extends commonApi
{

    function getBalance($walletInfo)
    {
        parent::getBalance($walletInfo);
        ini_set('max_execution_time', 300);

        $sendUrl = "https://etzscan.com/publicAPI?module=balance&action=balance&address=".$walletInfo['addr']."&tag=latest";
        $res = $this->sendCurl($sendUrl , $this->CI->timeKey);

        $intNum = sprintf('%F', $res['result']);
        $intNum = preg_replace('/\\.[0-9]+$/', '', $intNum);
        $balance = bcdiv($intNum, $this->CI->divisor, $walletInfo['coin_decimal']);

        return $this->setBalance($walletInfo , $balance , $this->CI->timeKey);
    }

    public function getTxList($walletInfo)
    {
        parent::getTxList($walletInfo);
        $param = $walletInfo['startBlock'];
        $coinType = $walletInfo['coin_type'];
        $tag_nm = $walletInfo['tag_nm'];
    }
}