<?php


class hdacApi extends commonApi
{
    public function getBalance($walletInfo)
    {
        parent::getBalance($walletInfo);

        $sendUrl = "https://explorer.as.hdactech.com/insight-api/addr/".$walletInfo['addr']."/?noTxList=1";
        $balance = $this->sendCurl($sendUrl , $this->CI->timeKey);

        return $this->setBalance($walletInfo , $balance['balance'] , $this->CI->timeKey);
    }

    public function getTxList($walletInfo)
    {
        parent::getTxList($walletInfo);
        $param = $walletInfo['startBlock'];
        $coinType = $walletInfo['coin_type'];
        $tag_nm = $walletInfo['tag_nm'];
    }

}