<?php


class qtumApi extends commonApi
{
    public function getBalance($walletInfo)
    {
        parent::getBalance($walletInfo);

        $sendUrl = "https://qtum.info/api/address/".$walletInfo['addr'];
        $res = $this->sendCurl($sendUrl , $this->CI->timeKey);

        $balance = bcdiv($res['balance'], $this->CI->divisor, $walletInfo['coin_decimal']);
        return $this->setBalance($walletInfo , $balance , $this->CI->timeKey);
    }

    public function getTxList($walletInfo)
    {
        parent::getTxList($walletInfo);
        $param = $walletInfo['startBlock'];
        $coinType = $walletInfo['coin_type'];
        $tag_nm = $walletInfo['tag_nm'];
    }
}