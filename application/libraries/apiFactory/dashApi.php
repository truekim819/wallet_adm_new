<?php


class dashApi extends commonApi
{

    function getBalance($walletInfo)
    {
        parent::getBalance($walletInfo);

        $sendUrl = "https://api.blockcypher.com/v1/dash/main/addrs/".$walletInfo['addr']."/balance";
        $res = $this->sendCurl($sendUrl , $this->CI->timeKey);

        $balance = isset($res['final_balance']) ? bcdiv($res['final_balance'], $this->CI->divisor, $walletInfo['coin_decimal']) : '0';

        return $this->setBalance($walletInfo , $balance , $this->CI->timeKey);
    }

    public function getTxList($walletInfo)
    {
        parent::getTxList($walletInfo);
        $param = $walletInfo['startBlock'];
        $coinType = $walletInfo['coin_type'];
        $tag_nm = $walletInfo['tag_nm'];

    }
}