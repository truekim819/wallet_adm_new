<?php


class btcApi extends commonApi
{

    public function getBalance($walletInfo){
        parent::getBalance($walletInfo);

        $sendUrl ='https://www.blockonomics.co/api/balance';

        $postParams = array(
            "sendUrl" => $sendUrl,
            'postData' => array('addr' => $walletInfo['addr'])
        );
        $res = $this->setPostCUrl($postParams);

        $balance =  '0' ;

        if( isset($res['response']) ){
            foreach (isset($res['response'])  as $val){
                $balance = bcadd($balance , $val['confirmed']);
                //$balance = bcadd($balance , $val['unconfirmed']);
            }
        }

        return $this->setBalance($walletInfo , $balance , $this->CI->timeKey);
    }


    public function getTxList($walletInfo)
    {
        parent::getTxList($walletInfo);
        $param = $walletInfo['startBlock'];
        $coinType = $walletInfo['coin_type'];
        $tag_nm = $walletInfo['tag_nm'];

        $postParam = array(
            "offset" => $param,
        );

        //BTC 주소에 대한 txid 리스트
        $bindQuery = http_build_query($postParam);
        $sendUrl = BTC_URL.$walletInfo['addr']."?".$bindQuery;

        $res = $this->sendCurl($sendUrl);

        $totalCnt = round($res['n_tx'],-1);

        if($param==0)
        {
            return $this->getTxList((int)$totalCnt - 40, $coinType , $tag_nm );
        }

        rsort($res->txs);
        $result = $res->txs;
        $txCount = count($result);

        if($txCount == 0){
            return $this->getTxList((int)$totalCnt - 40, $coinType , $tag_nm );
        }


        foreach ($result as $row)
        {
            $inSum = 0;
            $outSum = 0;
            $flag = false;
            $recieveSum = 0;

            foreach ($row['inputs'] as $inData)
            {
                if(!isset($inData['prev_out']))
                {
                    continue;
                }

                $inSum = bcadd($inData['prev_out']['value'], $inSum); //입금 합계
                $inAddr = $inData['prev_out']['addr'];

            }

            foreach ($row['out'] as $outData)
            {
                if(!isset($outData['addr']))
                {
                    continue;
                }

                $outSum  = bcadd($outData['value'] , $outSum); // 출금 합계

                $outAddr = $outData['addr'];

                if($outAddr == $walletInfo['addr']){
                    $recieveSum = bcadd($outData->value , $recieveSum);
                    $flag = true;
                }
            }

            if($flag){
                $outAddr = $walletInfo['addr'];
            }else{
                $inAddr  = $walletInfo['addr'];
            }

            //input = 보내는거 / out = 받는거
            $category = $flag ? "receive" : "send";

            $realTradAmt = $flag ? $recieveSum : 0 ;
            $realTradAmt = bcdiv($realTradAmt, BTC, 8);

            if($category == 'send'){
                $result = $this->setCurl('https://www.blockchain.com/btc/tx/' . $row->hash);

                $temp1 = explode('<td>Estimated BTC Transacted</td>', $result);
                $temp2 = explode('</tr>', $temp1[1]);
                $temp3 = str_replace(array('<td>', '</td>', '<span', '</span>', ' BTC'), '', $temp2[0]);

                $realTradAmt = explode('>', $temp3)[1];
                $realTradAmt = $realTradAmt[1];
                $realTradAmt = str_replace(',' ,'' , $realTradAmt);
            }

            $size = $row['size'];
            $tranFee = $inSum - $outSum;
            $fee = $tranFee / $size;

            $rgParams = array(
                "coinType" => "btc",
                "category" => $category,
                "parent" => $this->CI->coinInfo[0]['parent'],
                "mainType" => $this->CI->coinInfo[0]['main_type'],
                "subType" => $this->CI->coinInfo[0]['sub_type'],
                "tagNm"   => $walletInfo['tag_nm'],
                "blockNumber" => $row['block_height'],
                "from" => $inAddr,
                "to" => $outAddr,
                "txid" => $row['hash'],
                "amount" => $realTradAmt,
                "size" => $size,
                "fee" => $fee/BTC,
                "date" => date('Y-m-d H:i:s.u' , $row['time']),
            );

            $this->setTxList($rgParams);

        }

        $binds = array(
            ':coin_type'=> $coinType,
            ':tag_nm' => $tag_nm
        );
        $blockCnt = $this->getBlockCnt($binds);

        if($blockCnt >= $res['n_tx']  )
        {
            $retData['result'] = array(
                "success" => TRUE,
                "message" => "동기화 완료 되었습니다"
            );

            unset($this->CI->coinInfo);

            return $retData;
        }

        return $this->getTxList((int)$totalCnt - 40, $coinType , $tag_nm );
    }

    public function getBlock($binds){
        $blockCnt = $this->getBlockCnt($binds);

        return $blockCnt;
    }
}