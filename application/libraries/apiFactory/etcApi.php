<?php


class etcApi extends commonApi
{

    public function getBalance($walletInfo)
    {
        parent::getBalance($walletInfo);

        $sendUrl = "https://api.gastracker.io/addr/".$walletInfo['addr'];
        $res = $this->sendCurl($sendUrl , $this->CI->timeKey);

        $balance = $res['balance']['ether'];

        return $this->setBalance($walletInfo , $balance , $this->CI->timeKey);
    }


    public function getTxList($walletInfo)
    {
        parent::getTxList($walletInfo);
        $param = $walletInfo['startBlock'];
        $coinType = $walletInfo['coin_type'];
        $tag_nm = $walletInfo['tag_nm'];

    }

    public function getBlock()
    {
        $postParam = array(
            "module" => "block",
            "action" => "eth_block_number",
        );

        $bindQuery = http_build_query($postParam);

        $sendUrl = "https://blockscout.com/etc/mainnet/api?".$bindQuery;
        $res = $this->setCurl($sendUrl);
        $res = json_decode($res);
        $blockNumber = hexdec($res->result);
        return $blockNumber;
    }

}