<?php


class commonLib
{
    public $CI;

    public function __construct(){

        $this->CI = &get_instance();
        $this->CI->load->model('comModel');

    }

    public function __call($name, $arguments)
    {
        if(function_exists($name)){
            return $this->{$name}($arguments);
        }else{
            if(method_exists('comModel' , $name)){
                return $this->CI->comModel->{$name}($arguments);
            }
        }

    }

    public function getCoinAddress($coinType=null, $tagNm =null , $liveStatus =null){

        $result = $this->CI->comModel->getAddr($coinType , $tagNm, $liveStatus);

        return $result;
    }

    public function getCoinEct($coinType =null , $liveStatus =null){
        $result = $this->CI->comModel->getEct($coinType , $liveStatus);

        return $result;
    }

    public function getBalanceList($binds = null){
        //$groupkey = $this->CI->comModel->getGroupkey($binds);

        //$binds['groupkey'] = $groupkey;
        $temp = $this->CI->comModel->getBalanceList($binds);
        //var_dump($temp);
        //$liveList = $this->CI->comModel->getEct(null, 'Y' );
        //$liveList =  ($liveList , 'coin_type');
        $result = array();
        $defVal = number_format(0 , 18 );
        $wTypeList = array('W' => $defVal , 'D' => $defVal ,'T'=>$defVal, 'F'=>$defVal ,'S' => $defVal, 'E' => $defVal , 'R' => $defVal , 'Z' => $defVal);

        if(!empty($temp)) {
            foreach ($temp as $value) {

                if ( $value['main_type'] != 'C') {
                    continue;
                }

                if (!isset($result[$value['coin_type']])) {
                    $result[$value['coin_type']] = $wTypeList;
                }

                $result[$value['coin_type']][$value['wallet_type']] = number_format($value['sum_balance'], 18);
            }
        }
        return $result;
    }

    public function getCoinTxList($params){
        return $this->CI->comModel->selectTxList($params);
    }

    public function getCoinTxListCnt($params){
        $totCnt = $this->CI->comModel->selectTxListCnt($params);

        return $totCnt[0]['cnt'];
    }



    public function setAddrInfo($sendParams){

        $result = array(
            'result' => false
        );
        if($sendParams['saveType'] == 'Y'){
            $ret = $this->CI->comModel->setAddrInfo($sendParams);
        }else{
            $ectRet = $this->CI->comModel->setEctInfo($sendParams);

            if( $ectRet ){
                $ret = $this->CI->comModel->setAddrInfo($sendParams);
            }else{
                $result['message'] = "기타 정보 저장 중에 오류가 발생했습니다.";
                return $result;
            }
        }

        if(!$ret){
            $result['message'] = "주소 정보를 저장하지 못했습니다.";
        }else{
            $result['message'] = "주소 정보를 저장했습니다.";
            $result['result'] = true;
        }

        return $result;

    }

    public function getCoinDeposit($sendParam){

        $params = array(
            "mainType" => 'C',
            "category" => 'receive',
            "strDate" => $sendParam['strTime'],
            "endDate" => $sendParam['endTime'],
        );
        $result = $this->CI->comModel->getWalletDepositMoveAmt($params);
        $transferAmtList = array();

        $defVal = number_format(0 , 18 );

        foreach ( $result as $key => $val){
            if(!isset($transferAmtList[$val['coin_type']]['ctoc'] )) {
                $transferAmtList[$val['coin_type']]['ctoc'] = $defVal;
            }

            $amt = $transferAmtList[$val['coin_type']]['ctoc'];
            $transferAmtList[$val['coin_type']]['ctoc'] = bcadd($amt , $val['amt'] , 18);
        }

        unset($result);

        $result = $this->CI->comModel->getDepositSubTypeAmt($params);
        $typeList = array('F'=> 'found' , 'W'=>'htoc' , 'D' => 'htoc' , 'E'=>'ect' ,'Z' =>'ect' ,'S' =>'ect');

        foreach ( $result as $key => $val){

            $type = $typeList[$val['sub_type']];
            if( !isset($transferAmtList[$val['coin_type']][$type] )) {
                $transferAmtList[$val['coin_type']][$type] = $defVal;
            }

            $amt = $transferAmtList[$val['coin_type']][$type];
            $transferAmtList[$val['coin_type']][$type] = bcadd($amt , $val['amt'] , 18);
        }

        return $transferAmtList;
    }


    public function getCoinWithdraw($sendParam){

        $params = array(
            "mainType" => 'C',
            "category" => 'send',
            "strDate" => $sendParam['strTime'],
            "endDate" => $sendParam['endTime'],
        );

        $result = $this->CI->comModel->getWalletWithdrawMoveAmt($params);
        $transferAmtList = array();

        $defVal = number_format(0 , 18 );

        foreach ( $result as $key => $val){
            if(!isset($transferAmtList[$val['coin_type']]['ctoc'] )) {
                $transferAmtList[$val['coin_type']]['ctoc'] = $defVal;
            }

            $amt = $transferAmtList[$val['coin_type']]['ctoc'];
            $transferAmtList[$val['coin_type']]['ctoc'] = bcadd($amt , $val['amt'] , 18);
        }

        unset($result);

        $result = $this->CI->comModel->getWithdrawSubTypeAmt($params);
        $typeList = array('F'=> 'found' , 'W'=>'ctoh' , 'D' => 'ctoh' , 'E'=>'ect' ,'Z' =>'ect' ,'S' =>'ect');
        foreach ( $result as $key => $val){

            $type = $typeList[$val['sub_type']];
            if( !isset($transferAmtList[$val['coin_type']][$type] )) {
                $transferAmtList[$val['coin_type']][$type] = $defVal;
            }

            $amt = $transferAmtList[$val['coin_type']][$type];
            $transferAmtList[$val['coin_type']][$type] = bcadd($amt , $val['amt'] , 18);
        }

        return $transferAmtList;
    }

    public function setSync(){
        //전체 테이블 상태 체크
        $syncInfo = $this->CI->comModel->syncChoice();

        if(count($syncInfo) == 0)
        {
            //완료 된 코인 리스트를 다시 대기 상태로 변경
            $this->CI->comModel->changeNSync();
            $syncInfo = $this->CI->comModel->syncChoice();
        }

        $coinInfo = $this->CI->comModel->getAddr($syncInfo[0]['coin_type'], $syncInfo[0]['tag_nm']);

        if(!isset($coinInfo[0]) || $coinInfo[0]['parent'] != 'ETH'){
            $params = array(
                ":coin_type" => $syncInfo[0]['coin_type'],
                ":tag_nm" => $syncInfo[0]['tag_nm'],
                ":sync_state" => 'Y',
            );

            $this->CI->comModel->changeSync($params);

            return array(
                'sync'=>false ,
                'message' => "동기화 대상이 아닙니다."
            );
        }

        if($coinInfo[0]['main_type'] == 'H'){

            $params = array(
                ":coin_type" => $syncInfo[0]['coin_type'],
                ":tag_nm" => $syncInfo[0]['tag_nm'],
                ":sync_state" => 'Y',
            );

            $this->CI->comModel->changeSync($params);

            return array(
                'sync'=>false ,
                'message' => "동기화 대상이 아닙니다.(Hot Wallet)"
            );
        }

        //진행중인 동기화 갯수 체크
        $syncState = $this->CI->comModel->totCheckSync();
        if($syncState[0]['CNT'] > 3)
        {
            if($syncState[0]['DIFFTIME'] > 300)
            {
                $this->CI->comModel->syncDiffUpdate();
            }
            return array(
                'sync'=>false ,
                'message' => "동기화 중입니다."
            );
        }
        else
        {
            //$syncInfo[0]['coin_type'] = "WAVES";
            //$syncInfo[0]['tag_nm'] = "waves_cold_1";
            $params = array(
                ":coin_type" => $syncInfo[0]['coin_type'],
                ":tag_nm" => $syncInfo[0]['tag_nm'],
                ":sync_state" => 'S',
                "forUpdate" => true
            );


            $blockInfo = $this->CI->comModel->lastBlock($params);
            $param = (isset($blockInfo[0]['last_block']))? $blockInfo[0]['last_block'] : 0;
            $this->CI->comModel->changeSync($params);

            return array('sync'=>true , 'coinInfo' => $coinInfo[0] , 'param'=>$param , 'message'=>'동기화를 진행 합니다.');

        }
    }

    public function getLastBlock($coinType, $tagNm){
        $params = array(
            ":coin_type" => $coinType,
            ":tag_nm" => $tagNm
        );

        $blockInfo = $this->CI->comModel->lastBlock($params);

        return (isset($blockInfo[0]['last_block']))? $blockInfo[0]['last_block'] : 0;
    }

    public function getTop5List($params){
        return $this->CI->comModel->getTop5List($params);
    }

    public function setUserSign( $params ){

        $this->CI->load->model('procedure');

        $procResult = $this->CI->procedure->fnUserSign($params);

        $errMsg = array(
            1 => "성공",
            -1 => "쿼리 오류",
            -2 => "중복 User정보",
        );

        $result['result'] = array(
            "success" => FALSE,
            "message" => $errMsg[$procResult]
        );

        if($procResult == 1) {
            $result['result']['success'] = TRUE;
        }

        return $result;
    }

    public function loginSession( $params ){

        $this->CI->load->model('procedure');

        $procResult = $this->CI->procedure->fnLoginSession($params);

        $errMsg = array(
            1 => "로그인 성공",
            -1 => "쿼리 오류",
            -2 => "존재하지 않는 user 정보 입니다.",
            -3 => "해당 유저의 IP가 등록된 IP 와 일치하지 않습니다." ,
            -4 => "해당 회원의 비밀번호를 변경해야 합니다."
        );

        $result['result'] = array(
            "success" => FALSE,
            "message" => $procResult > 0 ? $errMsg[1] : $errMsg[$procResult],
            "procResult" => $procResult
        );

        if(!in_array($procResult , array( -1, -2, -3 ))) {

            $result['result']['success'] = TRUE;
            $userInfo =  $this->CI->comModel->getUserInfo(abs($procResult));
            $result['data'] = $userInfo[0];

        }

        return $result;
    }

}