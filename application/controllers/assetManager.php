<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class assetManager extends CI_Controller
{

    public function __construct($isView=true)
    {
        parent::__construct($isView);

        $this->load->library('commonLib');
        $this->load->view('common_body' , $this->viewParem);

    }

    public function index(){
        $viewParams = array();

        $viewParams['liveList'] = $this->commonlib->getCoinEct(null, 'Y');
        $viewParams['mainnetList'] = $this->commonlib->getMainnetList();

        $sendParam = array(
            'strDate' => date('Y-m-d H:i:s' , strtotime('-1 days')),
            'endDate' => date('Y-m-d H:i:s'),
            'strNo' => 0,
            'pageSize' => 100,
        );

        $viewParams['txList'] = $this->commonlib->getCoinTxList($sendParam);
        $viewParams['top5List'] = $this->commonlib->getTop5List($sendParam);

        $this->load->view(__FUNCTION__,$viewParams);
    }


    public function login(){
        $this->load->view('user/'.__FUNCTION__);
    }

    public function userRegist(){
        $this->load->view('user/'.__FUNCTION__ , $_GET);
    }

    public function coinBalance(){

        $searchDay = isset($_GET['searchDay']) && !empty($_GET['searchDay']) ? date('Ymd' ,strtotime($_GET['searchDay'].' 00:00:00')) : date('Ymd');
        $searchTime = isset($_GET['searchTime']) && !empty($_GET['searchTime']) ? $_GET['searchTime'] : date('H');

        $binds = array(
            "strTime" =>  date('YmdHis' ,strtotime( '-1 day',strtotime($searchDay.$searchTime .'0000'))),
            "endTime" => $searchDay.$searchTime.'0000',
        );

        $getBalance = $this->commonlib->getBalanceList($binds);
        $walletList = $this->commonlib->getCoinEct(null ,'Y');

        $coinList = array();
        if(!empty($walletList)) {
            foreach ($walletList as $key => $value) {
                if (!in_array($value['coin_type'], $coinList)) {
                    $coinList[] = $value['coin_type'];
                }
            }
        }

        $viewParams = array(
            'balanceList' => $getBalance  ,
            'liveList'=>$coinList,
            'searchDay' => date('Y-m-d' , strtotime($binds['endTime'])),
            'searchTime' => $searchTime
        );

        $this->load->view('coin/'.__FUNCTION__, $viewParams);
    }

    public function coinDaemon(){
        $viewParams = array();

        $walletList = $this->commonlib->getCoinEct(null , null , null);

        $coinList = array();
        if(!empty($walletList)) {
            foreach ($walletList as $key => $value) {
                if (!key_exists($value['coin_type'], $coinList)) {
                    $coinList[$value['coin_type']] = $value['live_yn'];
                }
            }
        }

        $strDate = isset($_GET['strDate']) ? $_GET['strDate'] : date('Y-m-d' , strtotime('- 7 days'));
        $endDate = isset($_GET['endDate']) ? $_GET['endDate'] : date('Y-m-d');

        $tabList = array();

        if(empty($strDate)) {
            for ($i = 0; $i < 7; $i++) {
                $tabList[] = date('Y-m-d', strtotime("- {$i}days"));
            }
        }else{
            $strTime = strtotime($strDate.'00:00:00');
            $endTime = strtotime($endDate.'00:00:00');

            for ($i = $endTime; $i >= $strTime ; $i=$i-86400) {
                $tabList[] = date('Y-m-d', $i);
            }

        }
        $viewParams['tabList'] = $tabList;
        $viewParams['walletList'] = $coinList;
        $viewParams['tarCoin'] = isset($_GET['cointype']) ? $_GET['cointype'] :  $walletList[0];
        $viewParams['strDate'] =$strDate;
        $viewParams['endDate'] =$endDate;
        $this->load->view('coin/'.__FUNCTION__,  $viewParams);
    }

    public function coinWithdraw(){
        $searchDay = isset($_GET['searchDay']) && !empty($_GET['searchDay']) ? $_GET['searchDay'] : date('Y-m-d');
        $searchTime = isset($_GET['searchTime']) && !empty($_GET['searchTime']) ? ' '.$_GET['searchTime'].':00:00' : date(' H:00:00');

        $sendParams = array(
            "strTime" =>  date('Y-m-d H:00:00' ,strtotime( '-1 day',strtotime($searchDay.$searchTime ))),
            "endTime" => $searchDay.$searchTime,
        );

        $viewParams = array(
            'searchDay' => $searchDay,
            'searchTime' => isset($_GET['searchTime']) && !empty($_GET['searchTime']) ? $_GET['searchTime']: date('H')
        );

        $viewParams['withdrawList'] = $this->commonlib->getCoinWithdraw($sendParams);
        $this->load->view('coin/'.__FUNCTION__,  $viewParams);
    }

    public function coinDeposit(){
        $searchDay = isset($_GET['searchDay']) && !empty($_GET['searchDay']) ? $_GET['searchDay'] : date('Y-m-d');
        $searchTime = isset($_GET['searchTime']) && !empty($_GET['searchTime']) ? ' '.$_GET['searchTime'].':00:00' : date(' H:00:00');

        $sendParams = array(
            "strTime" =>  date('Y-m-d H:00:00' ,strtotime( '-1 day',strtotime($searchDay.$searchTime ))),
            "endTime" => $searchDay.$searchTime,
        );


        $viewParams = array(
            'searchDay' => $searchDay,
            'searchTime' => isset($_GET['searchTime']) && !empty($_GET['searchTime']) ? $_GET['searchTime']: date('H')
        );

        $viewParams['depositList'] = $this->commonlib->getCoinDeposit($sendParams);

        $this->load->view('coin/'.__FUNCTION__,  $viewParams);
    }

    public function walletList(){
        $this->load->view('wallet/'.__FUNCTION__);
    }

    public function walletReg(){
        $this->load->view('wallet/'.__FUNCTION__);
    }


    public function test(){

        $hex = "0x07695a9144109111c000";
        $value = hexdec($hex);
        $value = sprintf('%f', $value);
        $balance = bcdiv($value, ETHER, 18);

        echo "$balance";

    }
}