<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class cron extends CI_Controller
{

    public function balance(){
        $this->load->view('api/balance');
    }

    public function dbSync(){
        $this->load->view('api/dbSync');
    }

}