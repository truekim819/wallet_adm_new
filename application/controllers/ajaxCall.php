<?php

class ajaxCall extends CI_Controller
{

    public function __construct($isView=false)
    {
        parent::__construct($isView);
        $this->load->library('commonLib');
    }

    public function balance(){

        $coinType = $_GET['coinType'] ;
        $walletInfo = $this->commonlib->getCoinAddress($coinType);
        $coinInfo = $this->commonlib->getCoinEct($coinType);

        foreach ($walletInfo as $value){
            if( strpos($value['tag_nm'],"_int_") ){
                continue;
            }

            $value['groupkey'] = $_GET['groupkey'];

            $token = $value['parent'];

            $libNm = strtolower($token).'Api';

            $this->load->library('apiFactory/'.$libNm);

            $value['coin_decimal'] = $coinInfo[0]['coin_decimal'];
            $value['extra_addr'] = $coinInfo[0]['extra_addr'];

            $res = $this->{strtolower($libNm)}->getBalance($value);
        }

        echo json_encode($res , 256);


    }

    public function fnPivx(){

        $libNm = 'pivxApi';

        $this->load->library('apiFactory/'.$libNm);
        $coinInfo['startBlock'] = 0;
        $coinInfo['groupkey'] = date('YmdHis');
        $res = $this->{strtolower($libNm)}->getTxList($coinInfo);

        echo json_encode($res , 256);
    }

    public function fnDaemonSync(){

        $coinType   = $_POST['coinType'];
        $tagNm      = $_POST['tagNm'];
        $startBlock = $_POST['startBlock'];

        $coinInfo = $this->commonlib->getCoinAddress($coinType, $tagNm);
        $coinEctInfo = $this->commonlib->getCoinEct($coinType, null , $coinInfo[0]['parent']);

        $coinInfo = array_merge($coinInfo[0] , $coinEctInfo[0]);

        if($coinInfo['parent'] !=='ETH'){
            //echo json_encode(array("message"=>"대상이 아닙니다." , "success" =>false) );
            //return ;
        }

        if($coinInfo['parent']=='EOS'){
            $startBlock = 0 ;
        }

        $token = $coinInfo['parent'];

        $libNm = strtolower($token).'Api';

        $this->load->library('apiFactory/'.$libNm);
        $coinInfo['startBlock'] = $startBlock;

        $res = $this->{strtolower($libNm)}->getTxList($coinInfo);

        echo json_encode($res , 256);
    }

    public function setSync(){
        $result = $this->commonlib->setSync();

        if(!$result['sync']){
            $result['success'] = $result['sync'];
            echo json_encode($result);
            exit;
        }

        $coinInfo = $result['coinInfo'];

        $libNm = strtolower($coinInfo['parent']).'Api';

        $this->load->library('apiFactory/'.$libNm);

        $res = $this->{strtolower($libNm)}->getTxList($result['param'] , $result['coinInfo']['coin_type'] , $result['coinInfo']['tag_nm']);

        echo json_encode($res);
    }

    public function getCoinTxList(){

        $coinType    = $_POST['coinType'];
        $tagNm       = isset($_POST['tagNm']) ? $_POST['tagNm'] : null;
        $page        = isset($_POST['page']) ? $_POST['page'] : null;
        $pageSize    = isset($_POST['pageSize']) ? $_POST['pageSize'] : null;
        $strDt       = isset($_POST['strDt']) ? $_POST['strDt'] : null;//date('Y-m-d' , strtotime('- 6 days'));
        $endDt       = isset($_POST['endDt']) ? $_POST['endDt'] : null;//date('Y-m-d') ;

        if( !empty( $coinType )){
            $listParams = array(
                "coinType" => $coinType,
                "tagNm" => $tagNm,
                "strNo" => !empty($page) ? ($page-1)*$pageSize : null,
                "pageSize" => $pageSize,
            );

            if( !empty($strDt) ){
                $listParams["strDate"]  = $strDt.' 00:00:00';
            }

            if( !empty($endDt) ){
                $listParams["endDate"]  = $endDt.' 23:59:59';
            }

            $viewParams['coinList'] = $this->commonlib->getCoinTxList($listParams);
            $viewParams['totCnt'] = $this->commonlib->getCoinTxListCnt($listParams);

            $end = $viewParams['totCnt'];
            if (!empty($page) && !empty($pageSize)) {
                $pageTotSize = ceil($end / $pageSize);

                $str = $page - 4;
                $last = $page + 5;

                if ($str < 1) {
                    $str = 1;
                    $last = 10;
                }

                if ($last > $pageTotSize) {
                    $last = $pageTotSize;
                }

                $pageList = array();
                for ($str; $str <= $last; $str++) {
                    $pageList[] = $str;
                }

                $viewParams['pageList'] = $pageList;
                $viewParams['pageTotSize'] = $pageTotSize;
            }
        }

        echo json_encode($viewParams, 256);

    }

    public function getAddrList(){
        $coinType = isset($_POST['coinType']) ? $_POST['coinType'] : null;
        $tagNm = isset($_POST['tagNm']) ? $_POST['tagNm'] : null;

        $res = $this->commonlib->getCoinAddress($coinType, $tagNm);

        echo json_encode($res, 256);
    }

    public function getCoinEctInfo(){
        $coinType = isset($_POST['coinType']) ? $_POST['coinType'] : null;

        $res = $this->commonlib->getCoinEct($coinType , null);

        echo json_encode($res, 256);
    }

    public function setAddrInfo(){

        $sendParams = array(
            "coinType" => !empty($_POST['coinType']) ? $_POST['coinType'] : null,
            "tagNm" => !empty($_POST['tagNm']) ? $_POST['tagNm'] : null,
            "addr" => !empty($_POST['addr']) ? $_POST['addr'] : null,
            "extraAddr" => !empty($_POST['extraAddr']) ? $_POST['extraAddr'] : null,
            "mainType" => !empty($_POST['mainType']) ? $_POST['mainType'] : null,
            "subType" => !empty($_POST['subType']) ? $_POST['subType'] : null,
            "parent" => !empty($_POST['parent']) ? $_POST['parent'] : $_POST['coinType'],
            "coinDecimal" => !empty($_POST['coinDecimal']) ? $_POST['coinDecimal'] : null,
            "apiKey" => !empty($_POST['apiKey']) ? $_POST['apiKey'] : null,
            "explorerUrl" => !empty($_POST['explorerUrl']) ? $_POST['explorerUrl'] : null,
            "liveYn" => !empty($_POST['liveYn']) ? $_POST['liveYn'] : null,
            "daemonYn" => !empty($_POST['daemonYn']) ? $_POST['daemonYn'] : null,
            "saveType" => !empty($_POST['saveType']) ? $_POST['saveType'] : null,
        );

        $result = $this->commonlib->setAddrInfo($sendParams);

        echo json_encode($result, 256);
    }

    public function getLastBlock(){
        $coinType = isset($_POST['coinType']) ? $_POST['coinType'] : null;
        $tagNm = isset($_POST['tagNm']) ? $_POST['tagNm'] : null;

        $lastBlock = $this->commonlib->getLastBlock($coinType, $tagNm);
        $coinInfo = $this->commonlib->getCoinAddress($coinType);

        if($coinInfo[0]['parent'] !=='ETH'){
            echo json_encode( array("message"=>"대상이 아닙니다." , "sync" =>false) );
            return ;
        }

        $token = $coinInfo[0]['parent'];

        $libNm = strtolower($token).'Api';

        $this->load->library('apiFactory/'.$libNm);

        $curBlock = $this->{strtolower($libNm)}->getBlock();

        $result = array(
            'syncBlock' => $lastBlock,
            'curBlock' => $curBlock,
            'sync' => true
        );
        echo json_encode($result, 256);
    }
    
    public function setUserSign(){

        $params = array(
            "userNm" => $_POST['userNm'],
            "userId" => $_POST['userId'],
            "userPw" => $_POST['userPw'],
            "groupCode" => $_POST['groupCode'],
        );

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        $params['joinIp'] = $ip;

        $res = $this->commonlib->setUserSign($params);

        echo json_encode($res, 256);

    }

    public function setLoginSession(){

        $userId = isset($_REQUEST['userId']) ? $_REQUEST['userId'] : null;
        $userPw = isset($_REQUEST['userPw']) ? $_REQUEST['userPw'] : null;

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        $sendParams = array(
            'userId' => $userId,
            'userPw' => $userPw,
            'userIp'  => $ip
        );

        $isUserInfo = $this->commonlib->loginSession($sendParams);

        if( $isUserInfo['result']['procResult'] > 0 ){

            $this->load->library('session');

            $this->session->set_userdata($isUserInfo['data']);

            $isUserInfo['result']['procResult'] = 1;
        }

        echo json_encode($isUserInfo);
    }

    public function delLoginSession(){

        if(!empty($this->loginInfos)){
            $this->session->sess_destroy();
            unset($this->loginInfos);
        }

        echo json_encode(array('success'=>TRUE , 'message'=>'로그아웃 됐습니다.'));
    }
}