<?php


class comModel extends CI_Model
{
    public function getAddr($coinType=null , $tagNm = null , $liveStatus = null){

        $sql = "
            SELECT 
                coin_type ,
                parent , 
                tag_nm , 
                main_type, 
                sub_type, 
                addr , 
                memo
             FROM coin_addr_info 
             WHERE 1=1 
        ";

        $binds = array();

        if(isset($coinType) && !empty($coinType)){
            if(is_array($coinType)) {

                $sql .= ' AND coin_type IN (';

                foreach ($coinType as $key => $val) {
                    $binds[':coinType' . $key] = $coinType;
                }

                $kList = array_keys($binds);
                $sql .= implode(',', $kList);
                $sql .= ') ';

            }else{
                $binds[':coinType'] = $coinType;
                $sql .= ' AND coin_type = :coinType ';
            }
        }

        if(!empty($tagNm)){

            $binds[':tagNm'] = $tagNm;
            $sql .= 'AND tag_nm = :tagNm ';

        }

        $sql .= 'ORDER BY coin_type ASC ';
        $coinArrList =  $this->execQuery($sql,$binds);

        return $coinArrList;
    }

    public function getEct( $coinType =null , $liveStatus =null ) {
        $sql = "SELECT coin_type , parent , coin_decimal , extra_addr , live_yn , daemon_yn , explorer_url , api_key  FROM coin_ect_info  ";

        $subQuery = array() ;
        $binds     = array() ;

        if( !empty( $coinType) ){
            $subQuery[]  = " coin_type = :coinType";
            $binds[':coinType'] = $coinType;
        }

        if( !empty( $liveStatus) ){
            $subQuery[]  = " live_yn = :liveStatus";
            $binds[':liveStatus'] = $liveStatus;
        }

        if( !empty($subQuery)) {
            $sql .= "WHERE ".implode(' AND ', $subQuery) ;
        }

        $sql .= ' ORDER BY coin_type ASC';

        return $this->execQuery($sql,$binds);
    }

    public function setCoinBalance($binds){

        $sql = "INSERT INTO
                coin_balance
                    (coin_type, main_type , wallet_type, tag_nm, balance, update_dt)
                VALUES
                    (:coinType , :mainType , :walletType , :tagNm , :balance , sysdate())
                on duplicate key
                    update balance = :balance  ,main_type = :mainType , update_dt = sysdate()";

        return  $this->execQuery($sql,$binds);
    }

    public function setCoinBalanceHistory($binds){

        $sql = "INSERT INTO
                coin_balance_history
                    (groupkey , coin_type, main_type ,wallet_type, tag_nm, balance, update_dt)
                VALUES
                    (:groupkey , :coinType , :mainType ,:walletType , :tagNm , :balance , sysdate())
                on duplicate key
                    update balance = :balance , update_dt = sysdate()";

        return  $this->execQuery($sql,$binds);
    }

    /**
     * @param array $binds
     * @return mixed
     */
    public function getBlockCnt($binds =array() ){

        $sql = "SELECT count(seq) as cnt FROM daemon_coin_log WHERE coin_type = :coin_type AND tag_nm = :tag_nm";

        $result =  $this->execQuery($sql,$binds);


        return $result[0]['cnt'];
    }
/*
    public function checkDaemon($params)
    {
        $sql = "SELECT count(txid) as CNT FROM daemon_coin_log WHERE txid = :txid AND tag_nm = :tag_nm";

        $binds = array(
            ':txid' => isset($params['hash']) ? $params['hash'] : $params['txid'],
            ':tag_nm' => $params['tagNm'],
        );

        if(!empty($params['category'])){
            $sql .= " AND category = :category";
            $binds[':category'] = $params['category'];
        }

        if(!empty($params['amount'])){
            $sql .= " AND amount = :amount";
            $binds[':amount'] = $params['amount'];
        }

        if(!empty($params['amount'])){
            $sql .= " AND from_addr = :from_addr";
            $binds[':from_addr'] = $params['from_addr'];
        }

        $qResult = $this->execQuery($sql,$binds);
        return $qResult;
    }

    public function insertDaemon($params){

        $sql = "INSERT INTO daemon_coin_log
				(coin_type, parent ,category , main_type , sub_type , tag_nm, blocknumber, from_addr, to_addr, txid, amount, fee, block_dt) 
			VALUES 
				(:coin_type, :parent, :category, :main_type , :sub_type , :tag_nm, :blocknumber, :from_addr, :to_addr, :txid, :amount, :fee, :block_dt)";

        $binds = array(
            ':coin_type' => $params['coinType'],
            ':parent' => $params['parent'],
            ':main_type' => $params['mainType'],
            ':sub_type' => $params['subType'],
            ':tag_nm' => $params['tagNm'],
            ':category' => $params['category'],
            ':blocknumber' => $params['blockNumber'],
            ':from_addr' => $params['from'],
            ':to_addr' => $params['to'],
            ':txid' => $params['txid'],
            ':amount' => $params['amount'],
            ':fee' => $params['fee'],
            ':block_dt' => $params['date'],
        );

        return $this->execQuery($sql,$binds);
    }
*/
    public function fnDaemonLogProc( $params ){
        $sql = "CALL DAEMON_LOG_PROC(:coin_type, :parent, :category, :main_type , :sub_type , :tag_nm, :blocknumber, :from_addr, :to_addr, :txid, :amount, :fee, :block_dt , @RESULT )";

        $binds = array(
            ':coin_type' => $params['coinType'],
            ':parent' => $params['parent'],
            ':main_type' => $params['mainType'],
            ':sub_type' => $params['subType'],
            ':tag_nm' => $params['tagNm'],
            ':category' => $params['category'],
            ':blocknumber' => $params['blockNumber'],
            ':from_addr' => $params['from'],
            ':to_addr' => $params['to'],
            ':txid' => $params['txid'],
            ':amount' => $params['amount'],
            ':fee' => $params['fee'],
            ':block_dt' => $params['date'],);

        $result = $this->execProc($sql,$binds);

        return $result;
    }

    /**
     * TxList 가져오는 쿼리
     * @param array $params
     * @return bool|mixed|mysqli_result
     */
    public function selectTxList($params = array()){

        //$table = "daemon_".$params['coinType'];

        $sql = "SELECT 
                * FROM ( SELECT seq, parent, coin_type, main_type, sub_type , tag_nm ,category,blocknumber,from_addr,to_addr, txid, amount, fee, block_dt  
                FROM daemon_coin_log WHERE 1=1";

        if(isset($params['coinType']) && !empty($params['coinType'])){
            $sql .= " AND coin_type = :coinType ";
        }

        if(isset($params['tagNm']) && !empty($params['tagNm'])){
            $sql .= " AND tag_nm = :tagNm ";
        }

        if(isset($params['strDate']) && !empty($params['strDate'])){
            $sql .= " AND block_dt >= :strDate";
        }

        if(isset($params['endDate']) && !empty($params['endDate'])){
            $sql .= " AND block_dt <= :endDate";
        }

        if(isset($params['mainType']) && !empty($params['mainType'])){
            $sql .= " AND main_type = :mainType";
        }

        if(isset($params['subType']) && !empty($params['subType'])){
            $sql .= " AND sub_type = :subType";
        }

        if(isset($params['category']) && !empty($params['category'])){
            $sql .= " AND category = :category";
        }

        if(isset($params['orderBy']) && !empty($params['orderBy']))
        {
            $sql .= ") a ORDER BY a.block_dt ";
        }
        else
        {
            $sql .= ") a ORDER BY a.block_dt DESC ";
        }

        if(isset($params['strNo'])){
            $sql .= " LIMIT :strNo , :pageSize ";
        }

        $binds = array(
            ':coinType' => isset($params['coinType']) && !empty($params['coinType'])? $params['coinType'] :null,
            ':tagNm'    => isset($params['tagNm']) && !empty($params['tagNm'])? $params['tagNm'] :null,
            ':strNo'    => isset($params['strNo']) && !empty($params['strNo']) ? intval($params['strNo']) : 0 ,
            ':pageSize' => isset($params['pageSize']) && !empty($params['pageSize']) ? intval($params['pageSize']) : 15 ,
            ':mainType' => isset($params['mainType']) && !empty($params['mainType'])? $params['walletType'] :null,
            ':subType' => isset($params['subType']) && !empty($params['subType'])? $params['subType'] :null,
            ':strDate' => isset($params['strDate']) && !empty($params['strDate'])? $params['strDate'] :null,
            ':endDate' => isset($params['endDate']) && !empty($params['endDate'])? $params['endDate'] :null,
            ':category' => isset($params['category']) && !empty($params['category'])? $params['category'] :null,
        );

        $txList =  $this->execQuery($sql,$binds);

        return $txList;
    }

    public function totDaemonCnt($params){

        $sql = "SELECT count(txid) as CNT, max(blocknumber) as blocknumber, max(block_dt) as reg_dt FROM daemon_coin_log WHERE tag_nm = :tag_nm ";

        $binds = array(
            ':tag_nm' => $params['tag_nm']
        );

        $qResult = $this->execQuery($sql,$binds);
        return $qResult;
    }

    public function getBalanceList($params = null){

        $sql = "SELECT substring(max(groupkey), 1 , 10) as gk ,coin_type , main_type, wallet_type, sum(balance) as sum_balance FROM coin_balance_history WHERE  balance > 0  AND groupkey IN ( SELECT max(groupkey) FROM coin_balance_history WHERE update_dt <= :endTime GROUP BY coin_type)  GROUP BY  substring(groupkey, 1 , 11) DESC , coin_type , main_type ,wallet_type ";

        $binds = array();
        $binds[':endTime'] = date('Y-m-d H:i:s' , strtotime($params['endTime']));

        $qResult = $this->execQuery($sql, $binds);

        return $qResult;
    }

    public function getGroupkey($params = null){

        $binds = array();
        if( isset($params) && !empty($params['strTime'])){
            $sql = "SELECT max(groupkey) as gk FROM coin_balance_history WHERE update_dt >=:strTime AND update_dt < :endTime  ORDER BY groupkey DESC limit 1";

            $binds[':strTime'] = date('Y-m-d H:i:s' , strtotime($params['strTime'].'0000'));
            $binds[':endTime'] = date('Y-m-d H:i:s' , strtotime($params['endTime'].'0000'));

        }else if( !empty($params['endTime']) ){

            $sql = "SELECT max(groupkey) as gk FROM coin_balance_history WHERE update_dt <= :endTime DESC limit 1";

            $binds[':endTime'] = date('Y-m-d H:i:s' , strtotime($params['endTime'].'0000'));

        }else{
            $sql = "SELECT max(groupkey) as gk FROM coin_balance_history ORDER BY groupkey DESC limit 1";
        }

        $result = $this->execQuery($sql, $binds);

        if(empty($result)){
            $newParams = array("endTime" => $params['endTime']);

            return $this->getGroupkey($newParams);
        }

        return $result[0]['gk'];
    }

    public function selectTxListCnt($params = array()){

        //$table = "daemon_".strtolower($params['coinType']);
        $sql = "SELECT 
                  count(*) as cnt
                FROM (SELECT * FROM daemon_coin_log WHERE 1=1";

        if(isset($params['coinType']) && !empty($params['coinType'])){
            $sql .= " AND coin_type = :coinType ";
        }

        if(isset($params['tagNm']) && !empty($params['tagNm'])){
            $sql .= " AND tag_nm = :tagNm ";
        }

        if(isset($params['strDate']) && !empty($params['strDate'])){
            $sql .= " AND block_dt >= :strDate";
        }

        if(isset($params['endDate']) && !empty($params['endDate'])){
            $sql .= " AND block_dt <= :endDate";
        }

        if(isset($params['walletType']) && !empty($params['walletType'])){
            $sql .= " AND wallet_type = :walletType";
        }

        if(isset($params['category']) && !empty($params['category'])){
            $sql .= " AND category = :category";
        }

        $sql .= ") as a";

        $binds = array(
            ':coinType' => isset($params['coinType']) && !empty($params['coinType'])? $params['coinType'] :null,
            ':tagNm'    => isset($params['tagNm']) && !empty($params['tagNm'])? $params['tagNm'] :null,
            ':walletType' => isset($params['walletType']) && !empty($params['walletType'])? $params['walletType'] :null,
            ':strDate' => isset($params['strDate']) && !empty($params['strDate'])? $params['strDate'] :null,
            ':endDate' => isset($params['endDate']) && !empty($params['endDate'])? $params['endDate'] :null,
            ':category' => isset($params['category']) && !empty($params['category'])? $params['category'] :null,
        );

        $txList =  $this->execQuery($sql,$binds);

        return $txList;
    }

    public function checkSync($params){
        $sql = "SELECT count(coin_type) as CNT FROM coin_sync WHERE coin_type = :coin_type AND tag_nm = :tag_nm ";

        $binds = array(
            ':coin_type' => $params['coinType'],
            ':tag_nm' => $params['tag_nm'],
        );

        $qResult = $this->execQuery($sql,$binds);
        return $qResult;
    }

    public function changeSync($binds){
        $sql = "UPDATE coin_sync SET sync_state = :sync_state, moddate = NOW(6) ";

        if(isset($binds[':last_block']) && !empty($binds[':last_block']))
        {
            $sql .= ", last_block = :last_block ";
        }
        else if(isset($binds[':last_block']) && is_numeric($binds[':last_block']))
        {
            $sql .= ", last_block = :last_block ";
        }

        $sql .= "WHERE coin_type = :coin_type AND tag_nm = :tag_nm ";

        return $this->execQuery($sql,$binds);
    }

    public function totCheckSync()
    {
        $sql = "SELECT count(coin_type) as CNT, TIME_TO_SEC(TIMEDIFF(now(6) , min(moddate))) as DIFFTIME FROM coin_sync where sync_state ='S'";

        $qResult = $this->execQuery($sql);
        return $qResult;
    }

    public function syncChoice()
    {
        $sql = "SELECT coin_type, tag_nm FROM coin_sync where sync_state ='N' limit 1 ";

        $qResult = $this->execQuery($sql);
        return $qResult;

    }

    public function changeNSync(){

        $sql = "UPDATE coin_sync SET sync_state = 'N' WHERE sync_state = 'Y'";

        $this->execQuery($sql);
    }

    public function lastBlock($binds){
        $sql = "SELECT last_block FROM coin_sync WHERE coin_type = :coin_type AND tag_nm = :tag_nm ";

        if(isset($binds['forUpdate']) && $binds['forUpdate']){
            //$sql .= " FOR UPDATE";
        }
        $qResult = $this->execQuery($sql, $binds);
        return $qResult;
    }

    public function syncDiffUpdate()
    {
        $sql = "UPDATE coin_sync SET sync_state = 'N' WHERE tag_nm in (SELECT * from (SELECT tag_nm FROM coin_sync WHERE sync_state = 'S' and TIME_TO_SEC(TIMEDIFF(now(6) , moddate)) > 300) as t)";

        return $this->execQuery($sql);
    }

    public function getEosTokenList()
    {
        $sql = "SELECT coin_type FROM coin_infos WHERE token = 'eos'";

        return $this->execQuery($sql);
    }

    public function setAddrInfo($sendParams){

        $sql = "INSERT INTO coin_addr_info ( coin_type , tag_nm , main_type , sub_type , parent , addr ) VALUES ( :coinType , :tagNm , :mainType , :subType , :parent , :addr ) " ;

        $binds = array(
            ":coinType" => $sendParams["coinType"],
            ":tagNm"    => $sendParams["tagNm"],
            ":mainType" => $sendParams["mainType"],
            ":subType"  => $sendParams["subType"],
            ":parent"   => $sendParams["parent"],
            ":addr"     => $sendParams["addr"],
        );

        return $this->execQuery($sql, $binds);
    }

    public function setEctInfo($sendParams){

        $sql = "INSERT INTO coin_ect_info ( coin_type , parent ,coin_decimal , extra_addr , live_yn , daemon_yn , explorer_url  , api_key ) 
                VALUES ( :coinType , :parent , :coinDecimal, :extraAddr , :liveYn , :daemonYn , :explorerUrl  , :apiKey );" ;

        $binds = array(
            ":coinType"     => $sendParams["coinType"],
            ":parent"       => $sendParams["parent"],
            ":coinDecimal"  => $sendParams["coinDecimal"],
            ":extraAddr"    => $sendParams["extraAddr"],
            ":liveYn"       => $sendParams["liveYn"],
            ":daemonYn"     => $sendParams["daemonYn"],
            ":explorerUrl"  => $sendParams["explorerUrl"],
            ":apiKey"       => $sendParams["apiKey"],
        );

        return $this->execQuery($sql, $binds);
    }


    public function updateEctInfo($sendParams){

        $sql = "UPDATE coin_ect_info 
                SET 
                    coin_decimal = :coinDecimal,
                    extra_addr = :extraAddr, 
                    live_yn = :liveYn, 
                    daemon_yn = :daemonYn, 
                    explorer_url  = :explorerUrl, 
                    api_key = :apiKey,
                    update_dt = CURRENT_TIMESTAMP 
                WHERE coin_type =:coinType AND parent = :parent";

        $binds = array(
            ":coinType"     => $sendParams["coinType"],
            ":parent"       => $sendParams["parent"],
            ":coinDecimal"  => $sendParams["coinDecimal"],
            ":extraAddr"    => $sendParams["extraAddr"],
            ":liveYn"       => $sendParams["liveYn"],
            ":daemonYn"     => $sendParams["daemonYn"],
            ":explorerUrl"  => $sendParams["explorerUrl"],
            ":apiKey"       => $sendParams["apiKey"],
        );

        return $this->execQuery($sql, $binds);
    }

    public function getDepositAnal(){
        $sql = "SELECT coin_type, sub_type , sum(amount) as sum_amt, sum(fee) as sum_fee FROM daemon_coin_log WHERE category ='receive' AND main_type='C' AND block_dt >= '2019-08-06 00:00:00.000000' AND block_dt < '2019-08-07 00:00:00.000000' GROUP BY coin_type , sub_type ";
    }

    public function getWalletDepositMoveAmt($params){

        $binds = array(
            ":mainType" => $params['mainType'],
            ":category" => $params['category'],
            ":strDate" => $params['strDate'],
            ":endDate" => $params['endDate'],
        );

        $subQuery = '';
        if(!empty($params['subType'])){
            $subQuery = " AND a.sub_type =:subType ";
            $binds[":subType"] = $params["subType"];
        }

        $sql = "SELECT substring(block_dt , 1, 10) as dt , coin_type, sum(amount) as amt
FROM daemon_coin_log 
WHERE block_dt >= :strDate  AND block_dt < :endDate 
AND coin_type is not null AND category = 'receive' AND from_addr IN ( SELECT distinct TRIM(addr) as addr FROM coin_addr_info WHERE main_type = 'C' ) 
GROUP BY substring(block_dt , 1, 10) , coin_type
";
        //

        return $this->execQuery($sql, $binds);
    }

    /**
     *
     * @param $params
     * @return bool|mixed|mysqli_result
     *
     */
    public function getDepositSubTypeAmt($params){

        $binds = array(
            ":mainType" => $params['mainType'],
            ":category" => $params['category'],
            ":strDate" => $params['strDate'],
            ":endDate" => $params['endDate'],
        );

        $subQuery = '';
        if(!empty($params['subType'])){
            $subQuery = " AND a.sub_type =:subType ";
            $binds[":subType"] = $params["subType"];
        }

        $sql = "SELECT substring(a.block_dt , 1, 10) as dt , a.coin_type, a.sub_type , sum(a.amount) as amt
                    FROM daemon_coin_log a 
                WHERE a.coin_type is not null AND a.category = :category $subQuery AND a.block_dt >= :strDate  AND a.block_dt < :endDate
                 AND a.from_addr NOT IN ( SELECT distinct TRIM(addr) as addr FROM coin_addr_info WHERE coin_type =a.coin_type AND main_type = :mainType)
                GROUP BY substring(a.block_dt , 1, 10),  a.coin_type , a.sub_type;";
//AND a.block_dt >= :strDate  AND a.block_dt < :endDate
        return $this->execQuery($sql, $binds);
    }

    public function getWalletWithdrawMoveAmt($params){

        $binds = array(
            ":mainType" => $params['mainType'],
            ":category" => $params['category'],
            ":strDate" => $params['strDate'],
            ":endDate" => $params['endDate'],
        );

        if(!empty($params['subType'])){
            $subQuery = " AND a.sub_type =:subType ";
            $binds[":subType"] = $params["subType"];
        }

        $sql = "SELECT substring(block_dt , 1, 10) as dt , coin_type, sum(amount) as amt
                FROM daemon_coin_log 
                WHERE block_dt >= :strDate  AND block_dt < :endDate 
                AND coin_type is not null AND category = 'send' AND to_addr IN ( SELECT distinct TRIM(addr) as addr FROM coin_addr_info WHERE main_type = 'C' ) 
                GROUP BY substring(block_dt , 1, 10) , coin_type";

        return $this->execQuery($sql, $binds);
    }

    public function getWithdrawSubTypeAmt($params){

        $binds = array(
            ":mainType" => $params['mainType'],
            ":category" => $params['category'],
            ":strDate" => $params['strDate'],
            ":endDate" => $params['endDate'],
        );

        $subQuery = '';
        if(!empty($params['subType'])){
            $subQuery = " AND a.sub_type =:subType ";
            $binds[":subType"] = $params["subType"];
        }

        $sql = "SELECT substring(a.block_dt , 1, 10) as dt , a.coin_type, a.sub_type , sum(a.amount) as amt
                    FROM daemon_coin_log a 
                WHERE a.coin_type is not null AND a.category = :category $subQuery AND a.block_dt >= :strDate  AND a.block_dt < :endDate
                 AND a.to_addr NOT IN ( SELECT distinct TRIM(addr) as addr FROM coin_addr_info WHERE coin_type =a.coin_type AND main_type = :mainType)
                GROUP BY substring(a.block_dt , 1, 10),  a.coin_type , a.sub_type;";
//
        return $this->execQuery($sql, $binds);
    }

    public function getMainnetList(){

        $sql = "SELECT * FROM coin_ect_info WHERE coin_type = parent ORDER BY coin_type ";

        return $this->execQuery($sql);
    }

    public function getTop5List($params){
        $sql = "SELECT * FROM (SELECT coin_type , category ,sum(amount) as sum_amt , count(1) as cnt FROM daemon_coin_log WHERE category = 'send' AND block_dt >= :strDate AND block_dt <= :endDate GROUP BY coin_type , category ORDER BY NULL) as a ORDER BY a.cnt DESC, a.sum_amt DESC limit 0 , 5";

        $binds = array(
            ":strDate" => $params['strDate'],
            ":endDate" => $params['endDate'],
        );

        return $this->execQuery($sql, $binds);
    }

    public function getWarmBalanceList($params){
        $sql = "SELECT * FROM (SELECT coin_type , category ,sum(amount) as sum_amt , count(1) as cnt FROM daemon_coin_log WHERE category = 'send' AND block_dt >= :strDate AND block_dt <= :endDate GROUP BY coin_type , category ORDER BY NULL) as a ORDER BY a.cnt , a.sum_amt DESC limit 0 , 5";

        $binds = array(
            ":strDate" => $params['strDate'],
            ":endDate" => $params['endDate'],
        );

        return $this->execQuery($sql, $binds);
    }

    public function getUserInfo($usn){
        $sql = "SELECT usn , user_name , group_code , grade , user_id ,join_ip, pw_change_dt FROM user_infos WHERE  usn=:usn";

        $binds = array(
            ":usn" => $usn
        );

        return $this->execQuery($sql, $binds);
    }

}