<?php


class procedure extends CI_Model
{
    public function fnDaemonLogProc( $params ){
        $sql = "CALL DAEMON_LOG_PROC(:coin_type, :parent, :category, :main_type , :sub_type , :tag_nm, :blocknumber, :from_addr, :to_addr, :txid, :trade_amt, :receive_amt , :sent_amt,  :fee, :vout , :block_dt , @RESULT )";

        $binds = array(
            ':coin_type'    => $params['coinType'],
            ':parent'       => $params['parent'],
            ':main_type'    => $params['mainType'],
            ':sub_type'     => $params['subType'],
            ':tag_nm'       => $params['tagNm'],
            ':category'     => $params['category'],
            ':blocknumber'  => $params['blockNumber'],
            ':from_addr'    => $params['from'],
            ':to_addr'      => $params['to'],
            ':txid'         => $params['txid'],
            ':trade_amt'    => $params['trade_amt'],
            ':receive_amt'  => $params['receive_amt'],
            ':sent_amt'     => $params['sent_amt'],
            ':fee'          => $params['fee'],
            ':block_dt'     => $params['date'],
            ':vout'         => $params['vout']
        );

        $result = $this->execProc($sql,$binds);
var_dump($result);
        return $result;
    }

    public function fnUserSign( $params ){
        $sql = "CALL USER_INFO_SAVE( :user_id ,:user_pw , :user_nm , :join_ip , :group_code , 1 , @RESULT )";

        $binds = array(
            ':user_id' => $params['userId'],
            ':user_pw' => $params['userPw'],
            ':user_nm' => $params['userNm'],
            ':join_ip' => $params['joinIp'],
            ':group_code' => (int) $params['groupCode'],
            );

        $result = $this->execProc($sql,$binds);

        return $result;
    }

    public function fnLoginSession( $params ){

        $sql = "CALL CHECK_LOGIN( :user_id ,:user_pw , :user_ip , @RESULT )";

        $binds = array(
            ':user_id' => $params['userId'],
            ':user_pw' => $params['userPw'],
            ':user_ip' => $params['userIp'],
            );

        $result = $this->execProc($sql,$binds);

        return $result;
    }

    public function fnCoinBalanceSave( $params ){

        $sql = "CALL COIN_BALANCE_PROC( :timekey , :tagnm , :balance_amt , :staking_amt , :confirm_amt , :unconfirm_amt ,  @RESULT )";

        $binds = array(
            ':timekey'       => $params['timeKey'],
            ':tagnm'         => $params['tagNm'],
            ':balance_amt'   => $params['balanceAmt'],
            ':staking_amt'   => $params['stakingAmt'],
            ':confirm_amt'   => $params['confirmAmt'],
            ':unconfirm_amt' => $params['unconfirmAmt'],
        );

        $result = $this->execProc($sql,$binds);

        return $result;
    }
}