<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIURL_BODYT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

define('API_KEY', 'NBDK9AQNGABBBJMEB5Z4AYXIU6P6TN7K35');
define('ETH_URL', 'https://api.etherscan.io/api?');

define('gwei' , bcpow(10, 9));
define('ETHER' , bcpow(10, 18));

define('ETH_ADDRESS' , '0x3f5CE5FBFe3E9af3971dD833D26bA9b5C936f0bE');

define('ETHER_API_KEY', 'NBDK9AQNGABBBJMEB5Z4AYXIU6P6TN7K35');
define('ETHER_URL', 'http://api.etherscan.io/api?');
define('ETHER_ADDRESS' , '0x3f5CE5FBFe3E9af3971dD833D26bA9b5C936f0bE');

define('BTC_URL', 'https://blockchain.info/rawaddr/');
define('BTC_TX_URL', 'https://blockchain.info/rawtx/');
define('BTC_ADDRESS', '1HAn2QW5TtH5vtwwxUs9Jcn4sGtzsyk6vc');
define('BTC_VALUE', bcpow(10, 8));

define('LTC_API_TOKEN', '1555a5ed900146cf8acab3e8ac4ec485');
define('LTC_URL', 'https://api.blockcypher.com/v1/ltc/main/addrs/');
define('LTC_TX_URL', 'http://chainz.cryptoid.info/coin/api.dws');
define('LTC_ADDRESS', 'LcM9XzTwLKsSp5J8GxZoeJrpKVZiadDexm');

#ETHER 계열 연산 단위
define('BTC' , bcpow(10, 8));
define('DBSERVER'       , 'localhost');
define('DBUSER'         , 'root');//계정은 root 말고 일반 유저 권한으로 생성해서 진행하죠
define('DBPW'           , '');//패스워드는 필수로 하고 대소문자+숫자+특수문자 포함해서 생성하죠.
define('DATABASE_NAME'  , 'daemon');

define('COMMON_API_PATH' , "C://xampp\htdocs\silvermoon\application\libraries\apiFactory\commonApi.php");
