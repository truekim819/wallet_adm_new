<body>
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-dark" style="background-color: #150054;">
            <a class="navbar-brand" href="/assetManager/index"><img src="/resource/main_logo.png" width="170px" height="56px"/></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav"  style="width:500px;">
                    <?php foreach ($menu as $title => $mInfo) { ?>
                        <li class="nav-item dropdown" style="width:250px;">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  style="color: #ff6d00">
                                <?php echo $title ?>
                            </a >
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <?php foreach ($mInfo as $subTit=>$url) { ?>
                                    <a class="dropdown-item" href = "<?=$url?>"><?=$subTit?></a >
                                <?php } ?>
                            </div >
                        </li >
                    <?php }?>
                </ul>
            </div>

            <div>
                <?php
                if( !isset($this->loginInfos['user_id']) ) {
                    ?>
                    <a style="color: #ff6d00" onclick="common.fnOpenNew('/assetManager/login' , '_self');">Login</a>
                    <?php
                }else{
                    ?>
                    <a style="color: #ff6d00" onclick="common.fnLogout()">Logout</a>
                    <?php
                }
                ?>
            </div>
        </nav>
    </div>
    <div id="subContents" style="padding:15px">