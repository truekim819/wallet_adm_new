<div class="jumbotron" style="background-color: #ffdcd2;">
    <h1 class="display-7" style="color: #910010">Wallet Regist</h1>
    <p class="lead">코인 지갑 정보 등록</p>
    <hr class="my-4">
    <div class="input-group mb-3" style="width:600px">
        <div class="input-group-prepend">
            <span class="input-group-text"  style="width:100px;">CoinType</span>
        </div>
        <input type="text" name="searchCoinType" class="form-control">
        <a class="btn btn-primary" href="#" onclick="fnGetCoinInfo();" role="button" style="width:200px;">코인정보 확인하기</a>
    </div>
    <hr class="my-4">
    <div id="helpMemo">
        <p style="color: #797979;">
            1. 지갑이름 : 테이블내 유니크 속성을 유지할 것
            </br>
            2. 지갑주소 : 실제 사용하는 주소 값
            </br>
            3. Extra Addr : ERC 코인의 경우 contract 주소를 기입 / 그 외에 본 주소값외의 구별값이 필요할 때 사용
            </br>
            4. Daemon/API : Daemon을 사용하는 경우 API 를 사용하는 경우를 잘 구분 지을 것
        </p>
    </div>
</div>
<form id="regInfo">
    <div class="input-group mb-3" id="mainnetType" style="display: none;">
        <div class="input-group-prepend">
            <span class="input-group-text">Mainnet Y/n</span>
        </div>
        <select class="form-control" name="mainnetType"></select>
    </div>

    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text">CoinType</span>
        </div>
        <input type="text" name="coinType" class="form-control" placeholder="ex.BTC/LTC/ETH ... " required>
    </div>

    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text">지갑이름</span>
        </div>
        <input type="text" name="tagNm" class="form-control" placeholder="ex.btc_cold_1" required>
    </div>

    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text">지갑주소</span>
        </div>
        <input type="text" name="addr" class="form-control" placeholder="" required>
    </div>

    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text">Extra Addr</span>
        </div>
        <input type="text" name="extraAddr" class="form-control" placeholder="ex> Erc = contract addr / trx token = token_id">
    </div>

    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text">C or H</span>
        </div>

        <select class="form-control" name="mainType" required>
            <option value="C" data-tag="cold">콜드월렛</option>
            <option value="H" data-tag="hot">핫월렛</option>
        </select>
    </div>

    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text">지갑종류</span>
        </div>
        <select class="form-control" name="subType" required>
            <?php
            $wTypeList = array('W'=>array('출금' , 'out' )  , 'D'=>array('입금' , 'in' ) , 'T'=>array('Staked' , 'staking') , 'F' => array('재단', 'found') , 'S'=>array('보관', 'storage')  , 'E' => array('기타' , 'etc') , 'R'=>array('리턴', 'return') , 'Z' =>array('비활성','notused') );
            foreach ($wTypeList as $type => $tit){
                echo "<option value='{$type}' data-tag='{$tit[1]}'> {$tit[0]} </option>";
            }
            ?>
        </select>
    </div>

    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text" >Parent</span>
        </div>

        <select class="form-control" id="selToken" name="parent" >
            <option value="">해당없음</option>
            <option value="ERC20">ERC-20 token</option>
            <option value="EOS">EOS token</option>
        </select>

        <input id="inputToken" type="text" class="form-control" placeholder="ex.ERC20 이면 ETH , EOS 토큰이면 EOS , 토큰이 아닌경우 CoinType " style="display:none;">

    </div>

    <div class="form-check">
        <input class="form-check-input" type="checkbox" value="" id="writeToken">
        <label class="form-check-label" for="defaultCheck1">
            직접입력
        </label>
    </div>

    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text">Decimal</span>
        </div>

        <input id="inputToken" type="number" name="coinDecimal" class="form-control">

    </div>


    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text">API KEY</span>
        </div>
        <input type="text" name="apiKey" class="form-control" placeholder="">
    </div>

    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text">Explorer Url</span>
        </div>
        <input type="text" name="explorerUrl" class="form-control" placeholder="">
    </div>

    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text">Live Y/N</span>
        </div>

        <select class="form-control" name="liveYn" required>
            <option value="N">N ( 라이브 중 아님 )</option>
            <option value="Y">Y ( 라이브 중 )</option>
        </select>
    </div>


    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text">Daemon/API</span>
        </div>

        <select class="form-control" name="daemonYn">
            <option value="N"> API 사용</option>
            <option value="Y"> Daemon 사용 </option>
        </select>
    </div>
    <input type="hidden" name="saveType" class="form-control" value="N">
</form>
<button type="button" class="btn btn-primary btn-lg btn-block" onclick="fnAddrSave();">등록하기</button>

<style>
    .input-group{
        width: 800px;
    }
    .input-group-text {
        width: 150px;
    }
</style>

<script>
    function fnGetCoinInfo(){
        var searchCoinType = $('[name=searchCoinType]').val().toUpperCase();

        if(searchCoinType == ""){
            return alert('코인타입을 입력하세요.');
        }

        $.ajax({
            url : '/ajaxCall/getCoinEctInfo',
            method : 'post',
            data : { coinType : searchCoinType },
            dataType : 'json',
            success : function(res){

                if( res.length > 0 ){
                    var coinInfo = res[0];

                    if(res.length > 1) {
                        var mainnet = $('[name=mainnetType]');
                        mainnet.empty();
                        $('#mainnetType').show();

                        $.each(res, function (i, e) {
                            coinInfo = e;
                            var opt = $('<option selected data-info="">'+ e['parent']+'</option>');
                            opt.attr('value' , e['parent']);
                            opt.data(  "info" ,  e  );
                            mainnet.append(opt);

                        });

                    }

                    $('[name=coinType]').val(coinInfo.coin_type);
                    $('[name=coinType]').attr('readonly' , 'readonly');

                    $('[name=extraAddr]').val(coinInfo.extra_addr);
                    $('[name=coinDecimal]').val(coinInfo.coin_decimal);

                    $('#selToken').hide();
                    $('#selToken').removeAttr("name");
                    $('#inputToken').attr("name", "parent");
                    $('#inputToken').show();

                    $('[name=parent]').val(coinInfo['parent']);

                    $('[name=apiKey]').val(coinInfo.api_key);
                    $('[name=explorerUrl]').val(coinInfo.explorer_url);

                    $('[name=liveYn]').val(coinInfo.live_yn);
                    $('[name=daemonYn]').val(coinInfo.daemon_yn);

                    $('[name=saveType]').val('Y');
                    reWriteTagNm();

                }else{
                    $('[name=coinType]').removeAttr('readonly');

                    $('[name=saveType]').val('N');
                    $('#regInfo').each(function(){
                        this.reset();
                    });

                    if(confirm("등록 하시려는 코인의 정보가 DB상에 존재하지 않습니다.\n\n 새로운 코인을 등록하시려면 확인 버튼을 눌러 주세요.")){
                        $('[name=coinType]').val(searchCoinType);
                        reWriteTagNm();
                    }
                }

            }
        });
    }

    function fnAddrSave(){
        var $checkList = $('#regInfo  input');
        var isValid = true;
        $.each($checkList , function(i , e){
            if($(e).prop("required")){
                $(e).css({'border-color' : "#ced4da"});
                if($(e).val() == ""){
                    $(e).css( { 'border-color' : 'red' } );
                    isValid = false;
                }
            }

        });

        if(!isValid)
        {
            alert("필수 값을 꼭 기입해 주세요.");
            return false;
        }

        var sendData = $('#regInfo').serialize();

        $.ajax({
            url : '/ajaxCall/setAddrInfo',
            method : 'post',
            data : sendData,
            dataType : 'json',
            success : function(res){
                alert(res.message);

            }
        });
    }

    $('#writeToken').change(function(){
        var isCheck = $('#writeToken').prop('checked');

        if(!isCheck){
            $('#selToken').show();
            $('#selToken').attr("name" , "parent");
            $('#selToken').prop("required" , true);
            $('#inputToken').hide();
            $('#inputToken').removeAttr("name");
            $('#inputToken').prop("required" , false);
        }else{
            $('#selToken').hide();
            $('#inputToken').attr("name" , "parent");
            $('#inputToken').prop("required" , true);
            $('#inputToken').show();
            $('#selToken').removeAttr("name");
            $('#selToken').prop("required" , false);
        }
    });

    $('[name=mainnetType]').change(function(){
        var info = $('[name=mainnetType] option:selected').data('info');

        $('[name=extraAddr]').val(info.extra_addr);
        $('[name=coinDecimal]').val(info.coin_decimal);

        $('#selToken').hide();
        $('#selToken').removeAttr("name");
        $('#inputToken').attr("name", "parent");
        $('#inputToken').show();

        $('[name=parent]').val(info['parent']);

        $('[name=apiKey]').val(info.api_key);
        $('[name=explorerUrl]').val(info.explorer_url);

        $('[name=liveYn]').val(info.live_yn);
        $('[name=daemonYn]').val(info.daemon_yn);

        $('[name=saveType]').val('Y');
        reWriteTagNm();

    });

    function reWriteTagNm(){
        var cointype = $('[name=coinType]').val().trim().toLowerCase();
        var mainType = $('[name=mainType] option:selected').data('tag').trim();
        var subType = $('[name=subType] option:selected').data('tag').trim();
        var tagNm = cointype+'_'+mainType+'_'+subType+'_';

        $('[name=tagNm]').val(tagNm);
    }

    $(document).ready(function () {
        $('[name=coinType]').keyup(function(event){
            if(event.keyCode !== 13 ){
                reWriteTagNm();
            }
        });

        $('[name=mainType] ,[name=subType]').change(function(){
            reWriteTagNm();
        })

    });
</script>
