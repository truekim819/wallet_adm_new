<div class="jumbotron" style="background-color: #ffdcd2">
    <h1 class="display-7" style="color: #910010">Wallet List</h1>
    <p class="lead">지갑 주소 및 정보 확인 창 </p>
    <hr class="my-4">
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text" style="width:100px;">코인명</span>
        </div>
        <input type="text" name="coinType" value="" class="form-control">
        <a class="btn btn-primary" href="#" onclick="fnSearch();" role="button" style="width:20%;">Search</a>
    </div>
    <div id="ectInfo">
        <span id="coinDecimal"></span>
        <span id="liveYn"></span>
    </div>
</div>

<div>
    <table class="table table-sm">
        <thead>
        <tr>
            <?php
            $headInfo = array('coinType'  , 'mainType' , 'subType' ,'tagName' , 'Address');
            foreach ($headInfo as $k => $t) {
                ?>
                <th scope="col"  style="border-style: solid;"><?= $t ?></th>
                <?php
            }
            ?>
        </tr>
        </thead>
        <tbody id="tb">

        </tbody>

    </table>
</div>

<div id="layout" style="display: none ; width:300px;background-color:whitesmoke " >
    <a onclick="$('#layout').hide()" style="float:right;">X</a>
    <ul id="addrInfo"></ul>
</div>

<script src='/resource/remark.js'></script>
<script>

    var coinType = null;
    var tagNm = null;
    var addrList = [];

    function getEctData( callback ){

        return new Promise( function (resolve){
            $.post('/ajaxCall/getCoinEctInfo', { coinType : coinType }).done(function(data){
                data = JSON.parse(data);
                resolve(data);
            });
        });
    }

    function getAddrData( callback ){

        return new Promise( function (resolve){
            $.post('/ajaxCall/getAddrList', { coinType : coinType , tagNm : tagNm }).done(function(data){
                addrList = JSON.parse(data);

                resolve(addrList);
            });
        });
    }



    function fnSearch() {
        coinType = $('[name=coinType]').val();

        if(coinType === "" ){
            alert("코인타입을 입력해 주세요.");
            return ;
        }

        coinType = coinType.toUpperCase();

        getEctData().then(function(res){
            var i = res[0];
            $('#coinDecimal').html('Decimal : <input type="text" value="'+i.coin_decimal+'">');
            $('#liveYn').html('live Y/N <input type="text" value="'+i.live_yn+'">');
        });

        tagNm = null;
        getAddrData().then(function(res){

            var tb = $('#tb');
            tb.empty();

            $.each(res , function (i , e ){

                if( i !== null){
                    var $tr = $('<tr></tr>');
                    $tr.append("<td>"+e.coin_type+"</td>");
                    $tr.append("<td>"+remark.mainTypeNmList[e.main_type]+"</td>");
                    $tr.append("<td>"+remark.subTypeNmList[e.sub_type]+"</td>");
                    $tr.append("<td>"+e.tag_nm+"</td>");
                    $tr.append("<td onclick='fnShowLayout(this);'><b>"+e.addr+"</b></td>");
                    tb.append($tr);
                }else{
                    tb.append("<tr><td colspan='6'>검색 결과가 존재하지 않습니다.</td></tr>");
                }

            });
        });
    }

    function fnShowLayout( tag ) {

        tagNm = $(tag).prev().html();

        var explorer_url = '';

        getEctData().then(function(res){
            explorer_url = res[0]['explorer_url'];

            getAddrData().then( function (addrInfo){
                var $ul = $('#addrInfo');
                $ul.empty();
                if(addrInfo.length > 0 ){

                    $.each(addrInfo , function (i , e ){
                        $ul.append("<li><a onclick='common.fnOpenNew(\""+explorer_url+e['addr']+"\")'>익스플로러 이동하기</a></li>");
                    });

                    var $offset = $(tag).offset();
                    $('#layout').offset({top:$offset.top , left : $offset.left+200});
                    $('#layout').show();
                }
            });
        });
    }


</script>