<div style="display: inline-block;">
    <div style="width:300px;display: inline-block;">
        <h4> 출금 중인 코인 목록 </h4>
        <table  class="table table-sm">
            <tr>
                <th>Coin Type</th>
                <th>Platform</th>
            </tr>
            <?php
            if( !empty($liveList) ) {
                foreach ($liveList as $live) {
                    ?>
                    <tr>
                        <td><?= $live['coin_type'] ?></td>
                        <td><?= $live['parent'] ?></td>
                    </tr>
                    <?php
                }
            }
            ?>
        </table>
    </div>


    <div style="width:300px;display: inline-block;margin-left:50px">
        <h4> 메인넷 코인 목록 </h4>
        <table  class="table table-sm">
            <tr>
                <th>Coin Type</th>
                <th>Platform</th>
            </tr>
            <?php
            if( !empty($mainnetList) ) {
                foreach ($mainnetList as $live) {
                    ?>
                    <tr>
                        <td><?= $live['coin_type'] ?></td>
                        <td><?= $live['parent'] ?></td>
                    </tr>
                    <?php
                }
            }
            ?>
        </table>
    </div>

    <div style="width:600px;display: inline-block;margin-left:50px">
        <h4> 현재 시간 입/출금 리스트 </h4>
        <table  class="table table-sm">
            <tr>
                <th>Coin Type</th>
                <th>Tag Name</th>
                <th>Category</th>
                <th>Amount</th>
            </tr>
            <?php
            if( !empty($txList) ) {
                foreach ($txList as $live) {
                    $cssCol = 'blue';
                    if ($live['category'] == 'send') {
                        $cssCol = 'red';
                    }
                    ?>
                    <tr style="color:<?= $cssCol ?>">
                        <td><?= $live['coin_type'] ?></td>
                        <td><?= $live['tag_nm'] ?></td>
                        <td><?= $live['category'] ?></td>
                        <td><?= $live['amount'] ?></td>
                    </tr>
                    <?php
                }
            }
            ?>
        </table>
    </div>

    <div style="width:600px;display: inline-block;margin-left:50px">
        <h4> 출금요청 탑5 리스트 </h4>
        <table  class="table table-sm">
            <tr>
                <th>Coin Type</th>
                <th>Total Amount</th>
                <th>Count</th>
            </tr>
            <?php
            if( !empty($top5List) ) {
                foreach ($top5List as $live) {
                    $cssCol = 'blue';
                    if ($live['category'] == 'send') {
                        $cssCol = 'red';
                    }
                    ?>
                    <tr style="color:<?= $cssCol ?>">
                        <td><?= $live['coin_type'] ?></td>
                        <td><?= $live['sum_amt'] ?></td>
                        <td><?= $live['cnt'] ?></td>
                    </tr>
                    <?php
                }
            }
            ?>
        </table>
    </div>
</div>
