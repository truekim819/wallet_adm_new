<div class="container" id="userRegister" style="float:left;width:50%">

    <label for="title"><b>회원 가입</b></label>

    <form id="regUserInfo" method="post">

    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text" style="width:100px;">회원 이름</span>
        </div>
        <input type="text" name="userNm" class="form-control" placeholder="">
    </div>

    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text" style="width:100px;">회원 부서</span>
        </div>
        <select name="groupCode" class="form-control">
            <option value="1">지갑관리실</option>
            <option value="2">재무회계실</option>
            <option value="3">운영실</option>
        </select>
    </div>


    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text" style="width:100px;"> I D </span>
        </div>
        <input type="text" name="userId" class="form-control" placeholder="" value="<?=isset($userId) ? $userId : null ?>">
        <button type="button" class="btn btn-primary" >중복검사</button>
    </div>


    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text" style="width:150px;"> 비밀번호 </span>
        </div>
        <input type="password" name="userPw" class="form-control" placeholder="">
    </div>


    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text" style="width:150px;"> 비밀번호 재확인 </span>
        </div>
        <input type="password" name="pwRe" class="form-control" placeholder="">
    </div>


    </form>
    <p>현재 가입하시려는 PC IP는
        <b style="color:red;">
            <?php
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $ip = $_SERVER['REMOTE_ADDR'];
            }

            echo $ip;
            ?>
        </b> 입니다.</p>

    <p>가입 후 해당 IP 외에 다른 IP에서 로그인 할 경우 IP 체크를 받게 됩니다.</p>

    <button type="button" class="btn btn-primary btn-lg btn-block" onclick="fnRegUser();">등록하기</button>

</div>

<script>

    var fnRegUser = function () {

        var sendData = $('#regUserInfo').serialize();

        $.ajax({
            url : '/ajaxCall/setUserSign',
            method : 'post',
            data : sendData,
            dataType : 'json',
            success : function(res){
                alert(res.result.message);
            }
        });
    }

</script>