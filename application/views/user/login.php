<div style="width:300px;margin: 20% 40%">
    <div class="input-group mb-3">
        <p> 접근 하신 사이트는 , IP와 ID를 기반으로 접근하신 페이지와 , 저장 및 업데이트 한 내역을 로그로 남기고 있습니다. </p>
    </div>
    <form id="loginForm" >
    <div class="input-group mb-3" >
        <div class="input-group-prepend">
            <span class="input-group-text" style="width: 100px;">I D </span>
        </div>

        <input name="userId" type="text" class="form-control" required>
    </div>

    <div class="input-group mb-3">

        <div class="input-group-prepend">
            <span class="input-group-text" style="width: 100px;">패 스 워 드 </span>

        <input  name="userPw" type="password" class="form-control" required>

    </div>
    </form>
    <div class="input-group mb-3">
        <button type="button" class="btn btn-lg" style="width:295px;" onclick="fnLogin();"> 로그인 </button>
    </div>

    <div class="input-group mb-3">
        <button type="button" class="btn btn-lg" style="width:295px;" onclick="fnUserSign();"> 회원가입 </button>
    </div>
</div>

<script>

    var fnLogin = function() {

        $.each($('#loginForm input'), function (i, e) {
            var $e = $(e).prop("required");

            if ($e) {
                if (!$(e).val().trim()) {
                    $(e).focus();
                    alert('필수 입력 값입니다.');
                    return false;
                }
            }
        });

        var loginPost = $('#loginForm').serialize();

        $.post('/ajaxCall/setLoginSession', loginPost, function (data) {

            var res = data.result;
            console.log(res);
            if (res.procResult < -3) {
                //비밀번호 변경 페이지로 이동
                fnOpenNew('', '_self');
            }

            alert(res.message);

            if( res.procResult > 0 ){
                history.back();
                //window.location.href = "/assetManager/index";
            }
        } , "json");
    }

    var fnUserSign = function(){
        var loginPost = 'userId=';

        if($('[name=userId]').val() != undefined){
            loginPost = loginPost+$('[name=userId]').val();
        }

        window.location.href = "/assetManager/userRegist?"+loginPost;
    }
</script>