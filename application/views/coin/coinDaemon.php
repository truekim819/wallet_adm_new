<div class="jumbotron" style="background-color: #ffdcd2">
    <h1 class="display-7" style="color: #910010">Daemon DB Sync</h1>
    <p class="lead">데몬디비 동기화 및 리스트 확인 창 </p>
    <hr class="my-4">
    <label for="exampleFormControlSelect1">Sync CoinType</label>
    <div class="form-group">
        <select class="form-control" id="coinType" style="width:20%;float: left;">
            <?php
            foreach ($walletList as $c => $value){
                $liveCol = "";
                if( $value == 'Y' ){
                    $liveCol = "#ff6d00";
                }
                ?>
                <option value='<?=json_encode(array('coinType'=>$c))?>' data-cointype="<?= $value?>" <?=$tarCoin == $c ? 'selected' : ''?> style="background-color: <?=$liveCol?>"><?= $c ?></option>
                <?php
            }
            ?>
        </select>

        <select class="form-control" id="tagNm" style="display:none;width:45%;float: left;"></select>
        <a class="btn btn-primary btn-sm" href="#" onclick="fnSyncSelect(this);" role="button" style="width:20%;">Select</a>
    </div>

    <div class="progress" style="display:none; background-color: white; width:100%; margin-bottom: 10px;">
        <div class="progress-bar progress-bar-striped progress-bar-animated" id="syncRateBar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
    </div>

    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text" style="width:100px;">시작날짜</span>
        </div>
        <input type="date" name="strDate" value="<?=$strDate?>" class="form-control">
        ~
        <div class="input-group-prepend">
            <span class="input-group-text" style="width:100px;">끝 날짜</span>
        </div>
        <input type="date" name="endDate" value="<?=$endDate?>" class="form-control">
        <a class="btn btn-primary" href="#" onclick="fnSearch();" role="button" style="width:20%;">Search</a>
    </div>

</div>
<div  class="card text-center" id="txList">
    <div class="card-header">
        <ul class="nav nav-tabs card-header-tabs">
            <?php

            foreach ($tabList as $menu  ){

                $actClass = '';
                if($menu == date('Y-m-d')){
                    $actClass = 'active';
                }
                ?>
                <li class="nav-item tablist">
                    <a class="nav-link <?=$actClass?>" onclick="fnMoveTab('<?=$menu?>' ,this )"><?=$menu?></a>
                </li>
                <?php
            }
            ?>
        </ul>
    </div>
</div>
<div id="topPage">
    <nav aria-label="Page navigation">
        <ul class="pagination justify-content-center">

        </ul>

    </nav>
    <!--button class="btn btn-primary btn-sm" id="excelDrop" href="#" role="button" style="width:20%;"> Excel </button-->
</div>
<table class="table table-sm" style="margin:20px;">
    <thead>
    <tr id="tbHead">
    </tr>
    </thead>
    <tbody id="tbBody"></tbody>
</table>

<div id="bottomPage">
    <nav aria-label="Page navigation">
        <ul class="pagination justify-content-center">

        </ul>
    </nav>
</div>

<script src='/resource/remark.js'></script>
<script>

    function fnSearch() {
        var strDate = $('[name=strDate]').val();
        var endDate = $('[name=endDate]').val();

        if(strDate > endDate){
            return alert('날짜설정이 잘못되었습니다.');
        }
        var jsonTxt = $('#coinType').val();
        var jsonObj = $.parseJSON(jsonTxt);

        window.location = '?cointype='+jsonObj.coinType+'&strDate='+strDate+'&endDate='+endDate;
    }

    function fnMoveTab($showCoin , tag ) {
        $('.tablist > a').removeClass('active');
        $(tag).addClass('active');
        $('#coinAddr').val('');

        $('#tbBody').empty();
        fnGetTxList($showCoin , 1);
    }

    function fnGetTxList($showCoin , page =1){

        var sendData = $('#coinType').val();
        var pageSize = 20
        sendData = $.parseJSON(sendData);
        $.ajax({
            url : '/ajaxCall/getCoinTxList',
            method : 'post',
            data :  {coinType : sendData.coinType , strDt : $showCoin , endDt : $showCoin , page: page , pageSize: pageSize },
            dataType : 'json',
            success : function(res){

                var nav = $('.pagination');
                nav.empty();

                if(res.coinList.length > 0){
                    var $head = $('#tbHead');
                    var $body = $('#tbBody');

                    $head.empty();
                    $body.empty();

                    $.each(res.coinList[0] , function( key ){

                        $head.append('<th scope="col">'+key+'</th>');
                    });

                    var seqNo = res.totCnt-((page-1)*pageSize);
                    $.each(res.coinList , function( idx , elem) {
                        var $tr = $('<tr></tr>');
                        var classDt  = elem.block_dt.substring(0 , 10);
                        $tr.addClass(classDt);

                        $.each(elem , function( key , e){
                            if(key === 'seq') {
                                $tr.append('<th scope="row">' + (seqNo--) + '</th>');
                            }else if( key == 'main_type'){
                                $tr.append('<td>'+ remark.mainTypeNmList[e]+'</td>');
                            }else if( key == 'sub_type'){
                                $tr.append('<td>'+ remark.subTypeNmList[e]+'</td>');
                            }else{
                                $tr.append('<td >' + e + '</td>');
                            }
                        });

                        $body.append($tr);
                    });

                    if(res.pageList.length > 0) {

                        if (page - 10 < 1) {
                            var $li = '<li class="page-item disabled"><a class="page-link" href="#">Prev</a></li>';
                        } else {
                            var $li = '<li class="page-item"><a class="page-link" href="#"  onclick="fnGetTxList(\''+$showCoin+'\','+ (page - 10) + ');">Prev</a></li>';
                        }

                        nav.append($li);

                        $.each(res.pageList, function (i, p) {
                            if (page == p) {
                                var $li = '<li class="page-item  active"><a class="page-link" href="#" onclick="fnGetTxList(\''+$showCoin+ '\','+ p + ');">' + p + '</a></li>';
                            } else {
                                var $li = '<li class="page-item"><a class="page-link" href="#" onclick="fnGetTxList(\''+$showCoin+ '\','+  p + ');">' + p + '</a></li>';
                            }
                            nav.append($li);
                        });

                        if (page == res.pageTotSize) {
                            var $li = '<li class="page-item disabled"><a class="page-link" href="#">next</a></li>';
                        } else if (page + 10 > res.pageTotSize) {
                            var $li = '<li class="page-item"><a class="page-link" href="#"  onclick="fnGetTxList(\''+$showCoin+ '\','+ res.pageTotSize + ');">Next</a></li>';
                        } else {
                            var $li = '<li class="page-item"><a class="page-link" href="#"  onclick="fnGetTxList(\''+$showCoin+ '\','+ (page + 10) + ');">Prev</a></li>';
                        }

                        nav.append($li);
                        $('#excelDrop').on('click' , {coinType : sendData.coinType , showDate : $showCoin } , fnDailyTxList );
                    }

                }else{
                    var $body = $('#tbBody');
                    $body.empty();
                    $body.append('<tr><td colspan="11"> 코인 이동에 대한 일일 데이터 정보가 없습니다.</td></tr>');
                }
            }
        });
    }

    function fnSelectChange(){
        var jsonTxt = $('#coinType').val();
        var jsonObj = $.parseJSON(jsonTxt);

        window.location = '?cointype='+jsonObj.coinType;
    }

    function fnSyncSelect(tag){
        var jsonTxt = $('#coinType').val();
        var jsonObj = $.parseJSON(jsonTxt);

        $.ajax({
            url : '/ajaxCall/getAddrList',
            method : 'post',
            data : jsonObj,
            dataType : 'json',
            success : function(res){
                if(res.length > 0){
                    var $select = $('#tagNm');
                    $('#tagNm').empty();

                    $.each(res , function ( i , e){
                        var $option = $('<option></option>');

                        $option.val(e.tag_nm);
                        $option.text(e.tag_nm+'   ====>    '+e.addr);
                        $select.append($option);
                    });
                    $select.show();

                    $(tag).text('Sync');
                    $(tag).off('click');
                    $(tag).on('click' , fnSync );
                    fnTagnmChange();
                }
            }
        });
    }

    function fnSync( ) {
        var jsonTxt = $('#coinType').val();
        var jsonObj = $.parseJSON(jsonTxt);

        $.ajax({
            url : '/ajaxCall/getCoinTxList',
            method : 'post',
            data :  {coinType : jsonObj.coinType , tagNm : $('#tagNm').val(), page: 1 , pageSize: 1},
            dataType : 'json',
            success : function(res){
                var blocknumber = 0 ;

                if(res.totCnt != "0"){
                    blocknumber = res.coinList[0].blocknumber;
                }
                $.ajax({
                    url : '/ajaxCall/fnDaemonSync',
                    method : 'post',
                    data :  {coinType : jsonObj.coinType , tagNm : $('#tagNm').val(), startBlock : blocknumber},
                    dataType : 'json',
                    success : function(syncRes){
                        if(syncRes.result.success == false)
                        {
                            alert(syncRes.result.message);
                        }
                        else
                        {
                            alert(syncRes.result.message);
                            location.reload();
                        }
                    }
                });
            }
        });

    }

    function fnDailyTxList(event){
        var data = event.data;

        common.fnOpenNew("/excel/coinDaemon?coinType="+data.coinType+'&strDate='+data.showDate+'&endDate='+data.showDate);
    }

    function fnTagnmChange(){
        var jsonTxt = $('#coinType').val();
        var jsonObj = $.parseJSON(jsonTxt);
        var tagNm   = $('#tagNm').val();

        $('.progress').hide();

        if(tagNm == ''){

            return false;
        }

        $.ajax({
            url : '/ajaxCall/getLastBlock',
            method : 'post',
            data :  {coinType : jsonObj.coinType , tagNm : tagNm },
            dataType : 'json',
            success : function(res){
                $('#syncRateBar').removeClass('bg-primary bg-success bg-warning bg-danger');
                $('.progress').show();
                var progressRate = 0;
                var color = '';

                if(res.sync){

                    if(res.syncBlock > 0){

                        progressRate = Math.floor((res.syncBlock/res.curBlock)*100);
                    }

                    if(progressRate > 90){
                        color = '';
                    }else if(progressRate > 70){
                        color = 'bg-success';
                    }else if(progressRate > 50){
                        color = 'bg-warning';
                    }else{
                        color = 'bg-danger';
                    }

                }else{
                    color = 'bg-danger';
                }

                $('#syncRateBar').addClass(color);
                $('#syncRateBar').attr('aria-valuenow' ,progressRate );
                if(progressRate == 0 ){
                    $('#syncRateBar').css({width:'100%'});
                }else{
                    $('#syncRateBar').css({width:progressRate+'%'});
                }
                if(!res.sync){
                    $('#syncRateBar').text('데몬디비 동기화를 지원하지 않는 코인입니다.');
                }else {
                    $('#syncRateBar').text('Sync Rate : ' + progressRate + '%');
                }
            }
        });
    }

    $(document).ready(function() {
        $('#coinType').change(fnSelectChange);
        fnGetTxList('<?=$tabList[0]?>');
        $('#tagNm').change(fnTagnmChange);

    });

</script>