<div class="jumbotron" style="background-color: #ffdcd2">
    <h1 class="display-7" style="color: #910010">Cold Wallet 정산</h1>
    <p class="lead">[출금] </p>
    <p  style="color: #000000"> 기준 날짜 : <b><?= isset($searchDay) && !empty($searchDay) ?  $searchDay : $searchDay=date('Y-m-d')?></b> </p>
    <p  style="color: #000000"> 기준 시간 : <b><?= isset($searchTime) && !empty($searchTime) ?  $searchTime : $searchTime=date('H')?>시</b></p>
    <hr class="my-4">
    <div class="input-group mb-3" style="width:50%">
        <div class="input-group-prepend">
            <span class="input-group-text" style="width:100px;">날짜</span>
        </div>
        <input type="date" name="toDate" value="<?= $searchDay ?>" class="form-control"  style="width:25%;">

        <div class="input-group-prepend">
            <span class="input-group-text" style="width:100px;">시간</span>
        </div>
        <select name="toTime"  style="width:25%;">
            <?php
            for($i = 0 ; $i < 24 ; $i++){
                $time = sprintf('%02d' , $i);
                if($time == $searchTime) {
                    echo "<option value='{$time}' selected>{$time}</option>";
                }else{
                    echo "<option value='{$time}'>{$time}</option>";
                }
            }?>
        </select>
        <a class="btn btn-primary" href="#" onclick="fnSearch();" role="button" style="width:20%;">Search</a>
    </div>

</div>

<table class="table table-sm">
    <thead>
    <tr>
        <th  rowspan="2">코인</th>
        <?php
        $wTypeList = array('Cold->Hot' , '지갑간 이동<br>(Cold<->Cold)' , '기타' => array('재단지원반납' , '대여' , '기타') , '출금합계');

        foreach ($wTypeList as $k => $t) {
            if(is_array($t)){ ?>
                <th scope="col" colspan="3"><?= $k ?></th>
                <?php
            }else {
                ?>
                <th scope="col" rowspan="2" style="border-style: solid;"><?= $t ?></th>
                <?php
            }
        }
        ?>
    </tr>
    <tr>
        <?php
        foreach ($wTypeList['기타'] as $k => $t) {
            ?>
            <th scope="col"  style="border-style: solid;"><?= $t ?></th>
            <?php
        }
        ?>
    </tr>
    </thead>
    </thead>
    <tbody>
    <?php
    foreach ($withdrawList as $k => $t) {
            echo "<tr>";
            ?>
            <th scope="col"><?=$k?></th>
            <td><?=isset($t['ctoh']) ? $t['ctoh'] : '0.0' ?></td>
            <td><?=isset($t['ctoc'])?  $t['ctoc']: '0.0'?></td>
            <td><?=isset($t['found']) ? $t['found']: '0.0'?></td>
            <td><?=isset($t['cash']) ? $t['cash']: '0.0'?></td>
            <td><?=isset($t['ect']) ? $t['ect']: '0.0'?></td>
            <?php
            $sum = 0;
            foreach ($t as $b){
                $sum = bcadd($b , $sum , 18);
            }
            echo "<td>".number_format($sum , 18, '.', ',')."</td>";
            echo "</tr>";
    }
    ?>
    </tbody>
</table>

<script>
    function fnSearch(){
        var searchDay = $('[name=toDate]').val();
        var searchTime = $('[name=toTime]').val();
        window.location.href = "?searchDay="+searchDay+"&searchTime="+searchTime;
    }
</script>