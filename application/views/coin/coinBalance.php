<div class="jumbotron" style="background-color: #ffdcd2">
    <h1 class="display-7" style="color: #910010">Cold Wallet 정산</h1>
    <p class="lead">[Daemon Balance] </p>
    <p  style="color: #000000"> 기준 날짜 : <b><?= isset($searchDay) && !empty($searchDay) ?  $searchDay : $searchDay=date('Y-m-d')?></b> </p>
    <p  style="color: #000000"> 기준 시간 : <b><?= isset($searchTime) && !empty($searchTime) ?  $searchTime : $searchTime=date('H')?>시</b></p>
    <hr class="my-4">
    <div class="input-group mb-3" style="width:50%">
        <div class="input-group-prepend">
            <span class="input-group-text" style="width:100px;">날짜</span>
        </div>
        <input type="date" name="toDate" value="<?= $searchDay ?>" class="form-control"  style="width:25%;">

        <div class="input-group-prepend">
            <span class="input-group-text" style="width:100px;">시간</span>
        </div>
        <select name="toTime"  style="width:25%;" >
        <?php
        for($i = 0 ; $i < 24 ; $i++){
            $time = sprintf('%02d' , $i);
            if($time == $searchTime) {
                echo "<option value='{$time}' selected>{$time}</option>";
            }else{
                echo "<option value='{$time}'>{$time}</option>";
            }
        }?>
        </select>
        <div class="input-group-prepend">
            <span class="input-group-text" style="width:100px;">Live Y/N</span>
        </div>
        <input type="checkbox"  class="form-control" id="liveYn">
        <a class="btn btn-primary" href="#" onclick="fnSearch();" role="button" style="width:20%;">Search</a>
    </div>

</div>
<table class="table table-sm">
    <thead>
    <tr>
        <th>코인</th>
        <?php
        $coinList = array_keys($balanceList);
        $wTypeList = array('W'=>'출금' , 'D'=>'입금' , 'T'=>'Staked', 'F' => '재단' , 'S'=>'보관'  , 'E' => '기타', 'R'=>'리턴' , 'Z' => '비활성'  ,'SUM' => '합계');

        foreach ($wTypeList as $k => $t) {
            ?>
            <th scope="col"  style="border-style: solid;"><?= $t.'('.$k.')' ?></th>
            <?php
        }
        ?>
    </tr>
    </thead>
    <tbody>

    <?php
    foreach ($balanceList as $coin => $bals) {
        $cssCol = '#ff6d00';
        $liveYn = 'N';
        $sum = number_format(0 , 18, '.' , ',');
        if(in_array($coin , $liveList)){
            $cssCol = '';
            $liveYn = 'Y';
        }
        echo "<tr  style='background-color:$cssCol' class='coin_balance {$liveYn}'>";
        echo "<td>$coin</td>";

        foreach ($bals as $types=> $bal) {
            if($bal > 0){
                $sum = bcadd( $sum , str_replace(',' , '' , $bal)  , 18);
            }

            $balance = explode('.' , $bal);
            $intVal = $bal > 0 ? "<b>$balance[0]</b>" : $balance[0];
            ?>
            <td><?=$intVal?>.<?=$balance[1]?></td>
            <?php
        }
        echo "<td>".number_format($sum , 18, '.', ',')."</td>";
        echo "</tr>";
    }
    ?>
    </tbody>

</table>

<script>
    function fnSearch(){
        var searchDay = $('[name=toDate]').val();
        var searchTime = $('[name=toTime]').val();
        window.location.href = "?searchDay="+searchDay+"&searchTime="+searchTime;
    }

    function fnLiveStatus(){
        var liveYn = $('#liveYn').prop("checked");

        if(liveYn){
            $('.coin_balance').hide();
            $('.Y').show();
        }else{
            $('.coin_balance').show();
        }
    }

    $('#liveYn').change(fnLiveStatus);
</script>