var common = {
    includeView : function ( path , id , params ) {
        $.ajax({
            url : path,
            method : "POST",
            context: document.body ,
            data : params,
            success: function(response){
                $("#"+id).empty();
                $("#"+id).append(response);
            },
            error : function(e){
                console.log(e);
            }
        });
    },
    getBalace : function(coinList , dt){
        if(typeof coinList == 'object') {
            $.each(coinList, function (i, e) {
                $.ajax({
                    url: '/ajaxCall/balance?coinType=' + e + '&groupkey=' + dt
                });
            });
        }else if(typeof coinList == 'string'){
            $.ajax({
                url: '/ajaxCall/balance?coinType=' + coinList + '&groupkey=' + dt
            });
        }
    },
    fnOpenNew : function (url, nm = '_blank') {
        window.open(url , nm);
    },


    fnLogout : function(){
        $.ajax(
            {
                url:  "/ajaxCall/delLoginSession",
                success : function(res){
                        alert("로그아웃 됐습니다.");
                        window.location.reload();
                    }
                }
        );
    }
};