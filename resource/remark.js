var remark = {
    coinList : [
        "ABT","ADA","ADD","AE","AMO","ANKR","APIS","ARN","ATD","AUTO","BAT","BCH","BHPC","BLACK"
        ,"BTC","BTG","BTT","BXA","BZNT","CHL","CMT","CON","CRO","CTXC","DAC","DACC","DASH","DENT"
        ,"ELF","ENJ","EOS","EOSDAC","ETC","ETH","ETHOS","ETZ","GNT","GTC","GTO","GXC","ITC"
        ,"KEOS","KNC","LAMB","LBA","LINK","LOOM","LRC","LTC","MCO","MEETONE","MITH","MIX","MTL","NPXS"
        ,"NULLS","OCN","OMG","ORBS","PAY","PCH","PIVX","PLY","POA20","POLY","POWR","PPT"
        ,"PST","QTUM","RDN","REP","RNT","ROM","SALT","SNT","STEEM","STRAT","THETA"
        ,"TMTG","TRUE","TRX","VALOR","VET","WAVES","WAX","WET","XEM","WTC"
        ,"XLM","XRP","ZEC","ZIL","ZRX","AOA","DTA","HYC"
        ,"ICX","INS","IOST","MCC","NKN","NULS","TFUEL","FX","CHR","QKC","MXC","HDAC" , "XVG",'FAB' ,'DVP', 'OGO' ,'TUNE' , 'FCT'
        /*
        "ABT","AE","AMO","ANKR","APIS","ARN","AUTO","BAT","BHPC"
        ,"BXA","BZNT","CON","CRO","CTXC","DAC","DACC","DENT"
        ,"ELF","ENJ","ETH","ETHOS","GNT","GTC","GTO","ITC"
        ,"KNC","LAMB","LBA","LINK","LOOM","LRC","MCO","MITH","MIX","MTL","NPXS"
        ,"OCN","OMG","ORBS","PAY","PCH","PLY","POA20","POLY","POWR","PPT"
        ,"PST","RDN","REP","RNT","ROM","SALT","SNT","TMTG","VALOR","WAX","WET","WTC"
        ,"ZRX","AOA","DTA","INS","MCC","NKN","NULS","FX","CHR","QKC","MXC",'FAB' ,'DVP', 'OGO'*/
    ],
    mainTypeNmList : {
        'H': '핫월렛', 'C' : '콜드월렛' , 'W' : '웜월렛'
    },
    subTypeNmList : {
        'W':'출금지갑' , 'D':'입금지갑' , 'T':'Staked', 'F' : '재단지갑' , 'S':'보관지갑'  , 'E' : '기타지갑', 'R':'리턴지갑' , 'Z' : '비활성지갑'
    },
};