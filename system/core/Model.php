<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2018, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2018, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Model Class
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		EllisLab Dev Team
 * @link		https://codeigniter.com/user_guide/libraries/config.html
 */
class CI_Model {


    public $CI;
	/**
	 * Class constructor
	 *
	 * @link	https://github.com/bcit-ci/CodeIgniter/issues/5332
	 * @return	void
	 */
    public function __construct()
    {
        $this->CI = &get_instance();
        if(!isset($this->db_con) && empty($this->db_con)){
            $this->db_con = $this->connectDB();
        }
    }

    public function __destruct()
    {
        $this->closeDB();
    }

    /**
	 * __get magic
	 *
	 * Allows models to access CI's loaded classes using the same
	 * syntax as controllers.
	 *
	 * @param	string	$key
	 */
	public function __get($key)
	{
		// Debugging note:
		//	If you're here because you're getting an error message
		//	saying 'Undefined Property: system/core/Model.php', it's
		//	most likely a typo in your model code.
		return get_instance()->$key;
	}


    public function connectDB( $connectDb = ''){

        if(!isset($this->db_con) || empty($this->db_con)){
            $database = isset($connectDb) && !empty($connectDb) ? $connectDb : DATABASE_NAME;
            $this->db_con = mysqli_connect(DBSERVER , DBUSER, DBPW , $database );

            if (!$this->db_con) {
                echo "Error: Unable to connect to MySQL." . PHP_EOL;
                echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
                echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
                exit;
            }
        }
    }

    public function closeDB(){
        if(isset($this->db_con) && !empty($this->db_con)){
            mysqli_close($this->db_con);
        }
    }

    public function execQuery( $query , $binds =array() ){
        if(!isset($this->db_con) || empty($this->db_con)){
            $this->connectDB();
        }

        if(!empty($binds)){
            $query = $this->bindParam($query , $binds);
        }

        $result = mysqli_query($this->db_con , $query);

        if(is_bool($result) == true){
            return $result;
        }else{
            return $result->fetch_all(MYSQLI_ASSOC);
        }
    }

    public function execProc( $query , $binds =array() ){
        if(!isset($this->db_con) || empty($this->db_con)){
            $this->connectDB();
        }

        if(!empty($binds)){
            $query = $this->bindParam($query , $binds);
        }

        mysqli_query($this->db_con , $query) or die("Query fail: " . mysqli_error());
        $result = mysqli_query($this->db_con , "SELECT @RESULT");
        $ret = mysqli_fetch_array($result);

        return $ret[0];
    }

    public function bindParam($sql , $bind){
        if(isset($bind) && !empty($bind) && is_array($bind) ){
            foreach($bind as $val){
                if(is_int($val) || strpos(strtolower($sql) , 'create')) {
                    $valList[] = $val;
                }else{
                    $valList[] = "'".$val."'";
                }
            }
            $query =  str_replace(array_keys($bind) , $valList , $sql);
        }
        return $query;
    }

}
